#pragma once
#include "Global.h"

class Deltager : public NumElement	//Arver fra NumElement
{
private:
	char * nasjon;	//Lagring for forkortelse
	string* navn;	//Navn p� deltager
	string* kjonn;	//Kjonn for deltager
public:
	Deltager() { };	//Default constructor, skal aldri kj�res
	Deltager(int* n);	//Oppretter ny deltager sortert p� nummer
	Deltager(ifstream & inn, int * nr);	//Leser inn deltager fra fil
	void display();	//Skriver ut all info om en deltager
	void endreDeltager(Deltager* tempdeltager);	//Endrer en deltager
	void omTropp(char * forkort);	//Skriver ut alle deltagere som h�rer til medsendt nasjon
	void deltagerSkrivTilFil(ofstream &inn);	//Skriver en deltager til fil
	void deltagerTilDeltagerListe(ofstream & utfil, Deltager *tempDeltager);	//Skriver deltager til deltagerliste
	void deltagerTilResultatListe(ofstream & utfil, Deltager *tempDeltager, int *plass);	//Skriver deltager til resultatliste
};