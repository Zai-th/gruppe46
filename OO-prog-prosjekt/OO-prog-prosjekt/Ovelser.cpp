#include "Ovelser.h"

//////////////�velse///////////
Ovelse::Ovelse(string * onavn, int * tempNr)
{
	nr = new int;
	*nr = *tempNr;							//Lager ny int or setter lik parameter
	++(*nr);								//plusser p� nr
	ovelseNavn = new string;
	ovelseNavn = onavn;
	cout << "\nAntall deltagere: ";		//sp�r om antall deltagere
	antDeltagere = new int;					//ny antall deltagerpointer
	cin >> *antDeltagere;					//Leser antall deltagere
	dato = new int;
	*dato = datofunk();						//Kj�rer datofunksjon p� dato
	klokkeslett = new int;
	*klokkeslett = klokkefunk();
}

////////////////Leser fra fil//////////////////
void Ovelse::lesOvelseFil(ifstream & innFil)
{
	nr = new int;
	innFil >> *nr;
	innFil.ignore();
	ovelseNavn = new string;				//Ny pointer
	getline(innFil, *ovelseNavn);			//Leser ovelsenavn
	antDeltagere = new int;
	innFil >> *antDeltagere;
	dato = new int;
	innFil >> *dato;
	klokkeslett = new int;
	innFil >> *klokkeslett;
}

///////////////Skriver �velse til fil/////////////////
void Ovelse::skrivOvelseFil(ofstream & utFil)
{
	utFil << *nr << endl;
	utFil << *ovelseNavn << endl;		//skriver �velsenavn etterfulgt av linjeskift
	utFil << *antDeltagere << endl;
	utFil << *dato << endl;
	utFil << *klokkeslett << endl;
}

//////////////Endre �velse//////////////////
void Ovelse::endreOvelse(Ovelse * tempOvelser)
{
	cout << "\nNavn: ";				//Sp�r om navn
	cin >> *ovelseNavn;				//leser navn
	*dato = datofunk();				//Kj�rer datofunksjon p� dato
	*klokkeslett = klokkefunk();	//Kj�rer klokkefunksjon p� klokkeslett
}

////////////////Display//////////////////
void Ovelse::display()
{
	cout << "\nNr: " << *nr;
	cout << "\nNavn: " << *ovelseNavn;				//Skriver ut �velsenavn
	cout << "\nAntall deltagere: " << *antDeltagere;
	cout << "\nDato: " << *dato;
	cout << "\nKlokkeslett: " << *klokkeslett << endl;
};

///////////////////Sjekk navn////////////////////
bool Ovelse::sjekkNavn(string * tempOvelseNavn)	//sjekker navn eksistens
{
	if (*ovelseNavn == *tempOvelseNavn)			//Hvis medsent parameter er lik �velsenavn
		return true;							//Returnerer true
	else										//Hvis ikke
		cout << "ERROR\n";						//Skriver feilmelding
}