#include "Grener.h"
#include "Gren.h"

///////////////Constructor///////////////
Grener::Grener()
{
	grenListe = new List(Sorted);		//Oppretter grenliste
}

//////////////Display funksjon/////////////
void Grener::display()
{
	grenListe->displayList();			//kj�rer displaylist p� grenliste
}

/////////////////Returnerer grenliste////////////////
List* Grener::giGrenListe()
{
	return grenListe;					//Bare brukt f�r � returnere grenliste
}

//////////////////Tell Gren/////////////////
void Grener::tellGren()
{
	antGrener++;						//Brukt for � plusse p� antall grener
}

//////////////////Skriv gren til fil////////////
void Grener::skrivGrenFil()
{
	ofstream grenFil("GRENER.DTA");						//Oppretter/�pner fil
	Gren * tempGren;									//Tempgren objekt
	int tempNoEle = grenListe->noOfElements();			//setter int til grenliste number of element
	if (grenFil)										//Hvis grenfil kan �pnes
	{
		grenFil << antGrener << endl;					//skriver antall grener
		for (int i = 1; i <= tempNoEle; i++)			//Kj�rer til ikke fler elementer
		{
			tempGren = (Gren*)grenListe->removeNo(i);	//fjerner gitt gren for � skrive til fil
			if (tempGren != NULL)
			{
				tempGren->grenSkrivTilFil(grenFil);		//Kj�rer grenskrivtilfil p� grenfil
				grenListe->add(tempGren);				//Legger tilbake pga skal bli brukt til �velse
			}
		}
		grenFil.close();								//Lukker fil
	}
	else cout << "\n�pning av NASJONER.DTA feilet...";	//Gir feilmelding om fil ikke kan �pnes
}