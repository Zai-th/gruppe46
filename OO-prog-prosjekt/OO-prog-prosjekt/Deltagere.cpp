#include "Global.h"
#include "Deltagere.h"
#include "Deltager.h"
///////////////Oppretter listen med deltagere///////////////
Deltagere::Deltagere()
{
	deltagerListe = new List(Sorted);	//Setter listen til sortert type
}
///////////////Returnerer listen med deltagere///////////////
List * Deltagere::gideltagerListe()
{
	return deltagerListe;
}
////////////////Skriver ut alle deltagerne i listen////////////
void Deltagere::display()
{
	deltagerListe->displayList();
}
////////////////F�r medsent antall deltagere og lagrer dette hos seg selv//////////////
void Deltagere::sendAntDeltager(int tempd)
{
	antDeltagere = tempd;	//Setter Deltagere sin antDeltagere lik medsendt parameter
}
/////////////////Plusser p� oversikten over antall deltagere/////////////
void Deltagere::tellDeltager()
{
	antDeltagere++;
}
////////////////Skriver alle deltagere i listen til fil/////////////
void Deltagere::skrivDeltagerTilFil()
{
	ofstream deltagerFil("DELTAGERE.DTA");	//�pner utfil for deltagere
	Deltager* temp;	//Mellomlagring for deltageren som skal skrives
	if (deltagerFil)	//Dersom filen kunnes �pnes
	{
		deltagerFil << antDeltagere << endl;	//Skriver antall deltagere til fil
		for (int i = 0; i < deltagerListe->noOfElements();)	//G�r igjennom alle deltakerne
		{
			temp = (Deltager*)deltagerListe->removeNo(0);	//Henter ut element 0
			if (temp != NULL)	//Sjekker at pekeren viser til en faktisk bruker
			{
				temp->deltagerSkrivTilFil(deltagerFil);	//Skriver deltageren til fil
				delete temp;
				temp = NULL;
			}
		}
		deltagerFil.close();	//Stenger filen
	}
	else //Dersom filen ikke kunne �pnes
		cout << "Kunne ikke opne fil!";
}