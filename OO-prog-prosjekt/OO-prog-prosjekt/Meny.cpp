#include "Meny.h"
#include "Global.h"

///////////////////////Hoved meny///////////////////////////
void hovedMeny()
{
	int tempantGren;
	char hmKommando;
	hmKommando = lesKommando();			//Kj�rer leskommando p� char

	while (hmKommando != 'Q')			//Kj�rer til Q blir lest inn
	{
		switch (hmKommando)
		{
		case 'N':	//Nasjon Meny
			nasjonMeny();
			break;
		case 'D':	//Deltager Meny
			deltagerMeny();
			break;
		case 'G':	//Grener Meny
			grenMeny();
			break;
		case 'O':	//�velser Meny
			tempantGren = antGren();	//Henter antall grener
			if (tempantGren > 0)		//Hvis antall grener er st�rre enn 0
			{
				skrivGrenTilFil();		//kj�rer skrivtilfil funksjon
				ovelseMeny();			//Kj�rer �velse meny
			}
			else						//Hvis antall grener er 0
				cout << "Registrer en gren!";	//Skriver feilmelding
			break;
		case 'M':
			nullStill();
			hentMedaljerOgPoeng();	//Oppdaterer medaljer
			skrivMedaljer();			//Skriv ut medaljer
			break;
		case 'P':
			nullStill();
			hentMedaljerOgPoeng();	//Oppdaterer poeng
			skrivPoeng();	//Skriver ut alle nasjoners poeng
			break;
		default: skrivHovedMeny();
		}
		hmKommando = lesKommando();
	}
};

/////////////////////Skriv Hoved meny////////////////////
void skrivHovedMeny()
{
	system("CLS");							//Clearer skjerm og skriver ut relevant informasjon
	cout << "\t\t\nHoved Menyen\n\n";
	cout << "N - Nasjoner\n";
	cout << "D - Deltagere\n";
	cout << "G - Grener\n";
	cout << "O - �velser\n";
	cout << "M - Medaljeoversikt\n";
	cout << "P - Poengoversikt\n";
	cout << "Q - Quit\n";
}

////////////////////Nasjon meny/////////////////////
void nasjonMeny()
{
	skrivNasjonMeny();				//skriver ut relevant informasjon
	char naKommando;
	naKommando = lesKommando();		//Kj�rer leskommando p� char

	while (naKommando != 'Q')		//Kj�rer s� lenge q ikke er lest inn
	{
		switch (naKommando)
		{
		case 'N':
			nyNasjon();				//Ny nasjon
			break;
		case 'E':
			endreNasjon();			//Endre nasjon
			break;
		case 'A':
			skrivNasjonListe();		//Skriver nasjonsliste
			break;
		case 'T':
			skrivDeltagere();		//Skriv deltagere
			break;
		case 'S':
			skrivEnNasjon();		//Skriv en nasjon
			break;
		default: skrivNasjonMeny();
		}
		naKommando = lesKommando();
	} skrivHovedMeny();
};

/////////////////////Skriv nasjonmeny////////////////////
void skrivNasjonMeny()
{
	system("CLS");							//Clearer skjerm og skriver ut relevant informasjon
	cout << "\t\t\nNasjon Menyen\n\n";
	cout << "N - Registrer en ny nasjon\n";
	cout << "E - Endre en nasjon\n";
	cout << "A - Skriv hoveddataene om alle nasjoner\n";
	cout << "T - Skriv en nasjons deltagertropp\n";
	cout << "S - Skriv alt av data om en nasjon\n";
	cout << "Q - Quit\n";
}

//////////////////////////Deltager meny//////////////////////////
void deltagerMeny()
{
	skrivDeltagerMeny();			//Skriver ut relevant informasjon
	char deKommando;
	deKommando = lesKommando();		//kj�rer leskommando p� char

	while (deKommando != 'Q')		//Kj�rer helt til q er lest inn
	{
		switch (deKommando)
		{
		case 'N':					//ny deltager
			nyDeltager();
			break;
		case 'E':
			endreDeltager();		//Endre deltager
			break;
		case 'A':
			skrivDeltagerListe();	//Skriver deltagerliste
			break;
		case 'S':
			skrivEnDeltager();		//skriv en deltager
			break;
		default: skrivDeltagerMeny();
		}
		deKommando = lesKommando();
	}
	skrivHovedMeny();
};

////////////////////////skriver deltagermeny//////////////////////////
void skrivDeltagerMeny()
{
	system("CLS");								//clearer skjerm og skriver ut relevant info
	cout << "\t\t\nDeltager Menyen\n\n";
	cout << "N - Registrer en ny deltager\n";
	cout << "E - Endre en deltager\n";
	cout << "A - Skriv hoveddataene om alle deltagere\n";
	cout << "S - Skriv alt av data on en deltager\n";
	cout << "Q - Quit\n";
}

///////////////////////////gren meny//////////////////////////
void grenMeny()
{
	skrivGrenMeny();					//Skriver ut relevant info
	char geKommando;
	geKommando = lesKommando();			//kj�rer leskommando

	while (geKommando != 'Q')			//Kj�rer til q er lest inn
	{
		switch (geKommando)
		{
		case 'N':
			nyGren();					//Ny gren
			break;
		case 'E':
			endreGren();				//Endre gren
			break;
		case 'A':
			skrivGrenListe();			//Skriv grenListe
			break;
		case 'S':
			skrivEnGren();				//Skriv en gren
			break;
		default: skrivGrenMeny();
		}
		geKommando = lesKommando();
	}
	skrivHovedMeny();
};

///////////////////////Skriv grenmeny/////////////////////
void skrivGrenMeny()
{
	system("CLS");							//Clearer skjerm og skriver ut relevant info
	cout << "\t\t\nGren Menyen\n\n";
	cout << "N - Registrer en ny gren\n";
	cout << "E - Endre en gren\n";
	cout << "A - Skriv hoveddataene om alle grener\n";
	cout << "S - Skriv alt av data on en gren\n";
	cout << "Q - Quit\n";
}

//////////////////////////�velse meny//////////////////////
void ovelseMeny()
{
	char * grenNvn;							//Oppretter pointer
	grenNvn = new char;
	bool sjekk = false;
	while (sjekk == false)					//S� lenge sjekk er false
	{
		cout << "Gren: ";					//Sp�r om gren
		cin >> grenNvn;						//Leser gren
		sjekk = finnesGren(grenNvn);		//Finnesgren kj�res p� input
	}
	int antOvelser = giAntOvelser(grenNvn);
	skrivOvelseMeny();						//Skriver relevant info
	char ovKommando;
	ovKommando = lesKommando();

	while (ovKommando != 'Q')				//Kj�rer til q er lest inn
	{
		switch (ovKommando)
		{
		case 'N':
			nyOvelse(grenNvn);				//Ny �velse
			antOvelser++;
			break;
		case 'E':
			endreOvelse(grenNvn);			//Endre �velse
			break;
		case 'A':
			skrivAlleOvelser(grenNvn);		//Skirv alle �velser
			break;
		case 'L':
			if (antOvelser != 0)
			startListeMeny(grenNvn);	//Start Liste meny
			else
				cout << "\nIngen ovelser er registrert.\n";
			system("pause");							//Pauser
			skrivOvelseMeny();							//Skriver meny
			break;
		case 'R':
			if (antOvelser != 0)
				resultatListeMeny(grenNvn);		//Resultat liste meny
			else
				cout << "\nIngen ovelser er registrert.\n";
			break;
		case 'F':
			int * ovelseNr;					//Oppretter pointer
			ovelseNr = new int;
			sjekk = false;
			cout << "�velse nummer: ";		//Sp�r om nummer
			cin >> *ovelseNr;				//Leser nummer
			sjekk = finnesOvelse(ovelseNr, grenNvn);		//Sjekker om �velse finnes ved bruk av navn og nummer
			if (sjekk == true)								//Hvis �velse finnes
				fjernOvelse(grenNvn, ovelseNr);				//Kj�rer fjern�velse med navn og nr
			else											//Hvis �velse ikke ble funnet
			{
				cout << "\nFant ikke ovelsen!\n";			//Skriver beskjed
				system("pause");							//Pauser
				skrivOvelseMeny();							//Skriver meny
			}
			break;
		default: skrivOvelseMeny();
		}
		ovKommando = lesKommando();
	}
	skrivHovedMeny();
}

///////////////////////////Skriv �velse meny/////////////////////////
void skrivOvelseMeny()
{
	system("CLS");							//Clearer skjerm og skriver ut relevant info
	cout << "\t\t\n�velse Menyen\n\n";
	cout << "N - Registrer en ny �velse\n";
	cout << "E - Endre en �velse\n";
	cout << "F - Fjern en �velse\n";
	cout << "A - Skriv hoveddataene om alle �velse\n";
	cout << "L - Deltagerliste\n";
	cout << "R - Resultatliste\n";
	cout << "Q - Quit\n";
}

//////////////////////////Startliste meny/////////////////////////
void startListeMeny(char * grenNvn)
{
	int * ovelseNr;						//Oppretter pointer
	ovelseNr = new int;
	bool sjekk = false;	//Om ovelse finnes
		cout << "�velse nummer: ";
		cin >> *ovelseNr;	//Tar input fra bruker
		sjekk = finnesOvelse(ovelseNr, grenNvn);	//Sjekker om �velse eksisterer
		if (sjekk != false)	//Dersom �velse er ekte
		{
			skrivStartListeMeny();	//Skriver meny for startlister
			char slmKommando;	//Lagring for input fra bruker
			slmKommando = lesKommando();	//Tar input fra bruker

			while (slmKommando != 'Q')	//S� lenge bruker ikke gir Q
			{
				switch (slmKommando)	//Switcher p� slmKommando
				{
				case 'S':
					if (sjekk == true)	//Dersom ovelse finnes
						skrivDeltagerStartListe(grenNvn, ovelseNr);	//Skriver deltagerlisten for gren med ovelsenr
					else //Dersom ovelse ikke finnes
					{
						cout << "\nListe finnes ikke.\n";
						system("pause");
						skrivStartListeMeny();	//Skriver forrige meny
					}
					break;
				case 'N':
					if (sjekk == true)	//Dersom ovelse finnes
						nyDeltagerListe(grenNvn, ovelseNr);	//Oppretter ny deltagerliste
					else //Dersom ovelse ikke finnes
					{
						cout << "\nListe finnes allerede.\n";
						system("pause");	//Gir bruker tid til � lese feilmelding
						skrivStartListeMeny();	//Skriver forrige meny
					}
					break;
				case 'E':
					if (sjekk == true)	//Dersom ovelse finnes
						endreDeltagerListe(grenNvn, ovelseNr);	//Endrer deltagerlisten
					break;
				case 'F':
					if (sjekk == true)	//Dersom ovelse finnes 
						fjernDeltagerListe(grenNvn, ovelseNr);	//Sletter listen for ovelse
					else
					{
						cout << "\nListe finnes ikke.\n";
						system("pause");	//Gir bruker tid til � lese feilmeldinger
						skrivStartListeMeny();	//Skriver forrige meny
					}
					break;
				default: skrivStartListeMeny();	//Skriver forrige meny
				}
				slmKommando = lesKommando();	//Tar imot input fra bruker
			}
		}
	skrivOvelseMeny();
}

//////////////////////Skriv startliste/////////////////////
void skrivStartListeMeny()
{
	system("CLS");							//clearer skjerm og skriver relevant informasjon
	cout << "\nStartliste Menyen\n\n";
	cout << "S - Skriv startliste\n";
	cout << "N - Ny startliste\n";
	cout << "E - Endre startliste\n";
	cout << "F - Fjern startliste\n";
	cout << "Q - Quit\n";
}

//////////////////////Resultatliste meny////////////////////
void resultatListeMeny(char * grenNvn)
{
	bool sjekk = false;
	int * ovelseNr;					//Oppretter ny pointer
	ovelseNr = new int;
	cout << "Ovelse nummer: ";		//Sp�r om nummer
	cin >> *ovelseNr;				//Leser nummer
	sjekk = finnesOvelse(ovelseNr, grenNvn);	//sjekker om �velse finnes
	if (sjekk != false)
	{
		skrivResultatListeMeny();		//Skriver meny
		char rlmKommando;
		rlmKommando = lesKommando();

		while (rlmKommando != 'Q')		//Kj�rer til q er lest inn
		{
			switch (rlmKommando)
			{
			case 'S':
				skrivResultatListe(grenNvn, ovelseNr);		//Skriver resultatlisten
				break;
			case 'N':
				if (sjekk == true)
					nyResultatListe(grenNvn, ovelseNr);		//Ny resultatliste
				else
					cout << "\nListe finnes allerede.\n";
				break;
			case 'F':
				fjernResultatListe(grenNvn, ovelseNr);		//Fjerner resultatliste
				hentMedaljerOgPoeng();
				break;
			default: skrivResultatListeMeny();
			}
			rlmKommando = lesKommando();
		}
	}
	else
		cout << "\nOvelse med gitt nummer finnes ikke.\n";
	system("pause");
	skrivOvelseMeny();
}

///////////////////////////Skriv resultatmeny///////////////////////
void skrivResultatListeMeny()
{
	system("CLS");							//Clearer skjerm og skriver relevant data
	cout << "\nResultatliste Menyen\n\n";
	cout << "S - Skriv resultatliste\n";
	cout << "N - Ny resultatliste\n";
	cout << "F - Fjern resultatliste\n";
	cout << "Q - Quit\n";
}