#pragma once
#include "Global.h"

//////////////////////Nasjon klasse/////////////////////
class Nasjon : public TextElement	//Arver fra TextElement
{
private:
	string * forkort;	//Lagring for forkortelse
	string * nasjonNavn;	//Lagring for nasjonsnavn
	string * kontaktNavn;		//Lagring for kontaktnavn
	int * kontaktTlf;	//Lagring for kontaktTLF
	int * antDeltagere;	//Lagring for antall deltagere
public:
	Nasjon() { };				//Tom constructor
	Nasjon(char* nvn);			//Constructor med forkortelse parameter
	char * giForkort() { return text; }	//Returnerer forkortelsen til nasjonen
	void endreNasjon(Nasjon * tempNasjon);		//Endre nasjon
	Nasjon(ifstream & inn, char * tempC);		//public nasjonfunksjoner
	void NasjonSkrivTilFil(ofstream & inn);		//Skriver nasjon til fil
	void display();								//Display funksjon
	~Nasjon();									//Deconstructor
};