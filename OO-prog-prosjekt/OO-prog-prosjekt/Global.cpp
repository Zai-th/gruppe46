#include "Global.h"
#include "Deltagere.h"
#include "Deltager.h"
#include "Nasjoner.h"
#include "Nasjon.h"
#include "Grener.h"
#include "Gren.h"
#include "Ovelser.h"
#include <string>
#include <fstream>
#include "Poeng.h"
#include "Medaljer.h"
//////////Globalt tilgjengelige objekter////////////
Nasjoner  nasjoner;
Deltagere deltagere;
Grener grener;
Poeng poenger;
Medaljer medaljer;
////////////////////Leser alt fra fil///////////////
void lesFraFil()
{
	bool bleOpnet = true;
	ifstream NasjonFil("NASJONER.DTA");	//Oppretter fraFil objekt
	if (NasjonFil) {	//Dersom filen ble �pnet
		int tempAnt = 0;	//Mellomlagring for antall nasjoner
		NasjonFil >> tempAnt;	//Fyller tempAnt fra fil
		if (tempAnt != 0) {	//Dersom det er minst en nasjon i filen
			nasjoner.mottaAnt(tempAnt);	//Sender antall nasjoner til nasjoner
			List * tempList = nasjoner.giNasjonListe();	//Mottar nasjonlisten ifra nasjoner
			char * forkort = new char;	//Mellomlagring for nasjonsforkortelse
			for (int counter = 0; counter < tempAnt; counter++) { //G�r igjennom alle nasjonene og fyller de fra fil
				NasjonFil >> forkort;	//Leser inn forkortelsen
				sendForkort(forkort);
				Nasjon * temp = new Nasjon(NasjonFil, forkort);	//Oppretter enkelt nasjon med forkortelsen som parameter
				tempList->add(temp);	//Legger den nye nasjonen in listen over nasjoner
			}
		}
		else
		{
			cout << "\nFil ikke funnet.\n";
			bleOpnet = false;
		}
		NasjonFil.close();	//Stenger fraFil objektet
		ifstream deltagerFil("DELTAGERE.DTA");	//Oppretter frafil objekt
		if (deltagerFil)	//Dersom filen ble �pnet
		{
			int antDeltagere = 0;	//Mellomlagring for antall deltagere
			deltagerFil >> antDeltagere;	//Fyller antDeltagere fra fil
			if (antDeltagere != 0)	//Dersom det finnes minst en deltaker
			{
				deltagere.sendAntDeltager(antDeltagere);	//Sender antall deltagere til deltagere
				int* nr = new int;	//Mellomlagring for number
				List* tempDList = deltagere.gideltagerListe();	//Henter ut listen over deltakere
				for (int i = 0; i < antDeltagere; i++)	//G�r igjennom og leser inn alle deltagere fra fil
				{
					deltagerFil >> *nr;	//Leser inn deltakerens number
					Deltager * tempD = new Deltager(deltagerFil, nr);	//Oppretter enkelt deltaker med number som parameter
					tempDList->add(tempD);	//Legger deltaker til i deltaker listen
				}
			}
			deltagerFil.close();	//Stenger fraFil objektet
		}
		else
		{
			cout << "Finner ikke fil!";	//Dersom fil ikke ble �pnet
			bleOpnet = false;
		}
		ifstream grenFil("GRENER.DTA");	//Oppretter fraFil objekt
		if (grenFil) {	//Dersom fil ble �pnet
			int antGrener = 0;	//Mellomlagring for antall grener
			grenFil >> antGrener;	//Henter ut totalt antall grener fra fil
			if (antGrener != 0)	//Dersom det finnes minst en gren
			{
				grener.mottaAnt(antGrener);	//Sender antall grener til grener objektet
				string * grenNvn = new string;	//Mellomlagring for navnet p� gren
				List * tempGList = grener.giGrenListe();	//Henter ut listen over grener
				for (int i = 0; i < antGrener; i++)	//G�r igjennom alle grener og leser inn disse fra fil
				{
					grenFil.ignore();	//Ignorerer blank
					getline(grenFil, *grenNvn);	//Henter ut navnet p� gren
					const char* grenNavn = grenNvn->c_str();	//Konverterer string til char
					Gren * tempG = new Gren(grenFil, grenNavn);	//Oppretter ny gren fra fil med grenNavn som parameter
					tempGList->add(tempG);	//Legger grenen i listen med grener
				}
			}
			else
			{
				cout << "Fil ikke funnet.";
				bleOpnet = false;
			}
			grenFil.close();	//Stenger inFil objektet
			if (bleOpnet == true)
				hentMedaljerOgPoeng();	//Leser inn fra alle resultatlister og legger i poeng
		}
	}
}
///////////////////Skriver alle nasjoner, deltakere og grener til fil////////////////
void skrivTilFil() 
{
	nasjoner.skrivNasjonFil();	//Skriver nasjoner til fil
	deltagere.skrivDeltagerTilFil();	//Skriver deltagere til fil
	grener.skrivGrenFil();	//Skriver grener til fil
}

/////////////////////////////////////////////NASJON/////////////////////////////////////////////////
//////////////////////Oppretter en ny nasjon//////////////////
		void nyNasjon() 
		{
			char* nvn = new char[STRLEN];	//Mellomlagring for forkortelse
			cout << "Skriv forkortelse for nasjon: ";
			cin >> nvn;	//Mottar forkortelse fra bruker
			sendForkort(nvn); //Sender forkortelse til statistikk klasse
			List * templist = nasjoner.giNasjonListe();	//Henter ut listen over nasjoner
			if (!templist->inList(nvn))	//Dersom nasjonen ikke allerede finnes
			{
				Nasjon * nasjon = new Nasjon(nvn);	//Oppretter ny nasjon med forkortelse som parameter
				templist->add(nasjon);	//Legger til ny nasjon i listen over nasjoner
				nasjoner.tellNasjon();	//�ker antall nasjoner med 1
			cout << "\nNasjon lagt til.\n";
			}
			else cout << "\nDenne forkortelsen er allerede i bruk.\n";	//Dersom nasjon alt finnes
			system("pause");	//Pauser program s� meldinger kan leses
			skrivNasjonMeny();	//Skriver menyen til nasjoner
		}
////////////////////////Endrer en nasjon////////////////////
		void endreNasjon() {
			bool iListe = false;	//Om nasjonen finnes
			List * tempList = nasjoner.giNasjonListe();		//Henter ut listen med nasjoner
			char * nvn = new char[20];	//Mellomlagring for forkortelse
			
				cout << "Skriv inn forkortelse for nasjon: ";
				cin >> nvn;	//Mottar forkortelse fra bruker
				iListe = tempList->inList(nvn);	//Lagrer svar p� s�k etter forkortelse i nasjonlisten
				if (iListe == false)	//Dersom forkortelse ikke finnes i liste
				{
					cout << "Ugyldig forkortelse.\n";
					system("pause");
					skrivNasjonMeny();	//Skriver nasjoner sin meny
				}
				else
				{
					Nasjon * tempNasjon = (Nasjon*)tempList->remove(nvn);	//Henter ut nasjonen med gitt forkortelse
					tempNasjon->endreNasjon(tempNasjon);	//Kj�rer endre nasjon
					tempList->add(tempNasjon);	//Legger tilbake i listen
					skrivNasjonMeny();	//Skriver nasjoner sin meny
				}
		}
		/////////////////Sjekker om en nasjon eksisterer////////////////
		bool finnesNasjon(char * nasj)
		{
			List * temp = nasjoner.giNasjonListe();	//Henter ut listen med nasjoner
			if (temp->inList(nasj))	//Dersom nasjonsforkortelsen som er medsendt finnes i listen
			{
				return true;
			}
			else
				cout << "Nasjon finnes ikke!";
			return false;
		}
		/////////////////////////Gir fra seg listen over nasjoner///////////////////
		void hentNasjonListe(List temp) {
			nasjoner.giNasjonListe();	//Sender fra seg listen over nasjoner
		}
		///////////////////////Skriver ut alle nasjoner////////////////////
		void skrivNasjonListe() {
			nasjoner.display();	//Skriver ut absolutt alle nasjoner
			skrivNasjonMeny();	//Printer menyen for nasjoner
		}
		///////////////////Skriver data om en eneste nasjon///////////
		void skrivEnNasjon() {
			char * forkort = new char;	//Mellomlagring for forkortelser
			List * tempList = nasjoner.giNasjonListe();	//Henter ut listen over nasjoner
			bool finnes = false;	//Lagring for om en nasjon finnes
				cout << "Gi forkortelse: ";			//Sp�r om forkortelse
				cin >> forkort;						//Leser forkortesle
				finnes = tempList->inList(forkort);	//Sjekker p� templist om forkortelse finnes
				if (finnes == false)				//Hvis forkortelse ikke finnes
				{
					cout << "Ugyldig forkortelse\n";	//skriver feilmeding, en pause og relevant meny
					system("pause");
					skrivNasjonMeny();
				}
				else									//Hvis forkortelse ikke finnes
				{
					Nasjon * nasjon = (Nasjon*)tempList->remove(forkort);	//Fjerner nasjon fra liste
					nasjon->display();										//Kj�rer display funksjon p� valgt nasjon
					tempList->add(nasjon);	//Legger tilbake i listen
					system("pause");
					skrivNasjonMeny();	//Skriver nasjoner sin meny
				}
		}

		int giAntNasjoner()
		{
			int ant = nasjoner.giAnt();
			return ant;
		}

		///////////////////////////////////////////////////DELTAGER////////////////////////////////////////////

		//////////////////////Ny Deltager///////////////////////////
		void nyDeltager()
		{
			int* nr = new int;								//Oppretter ny int pointer
			cout << "Deltagerens nummer: ";					//Sp�r om deltagerens nr
			*nr = sjekkInt();								//Leser deltagerens nr
			List * templist = deltagere.gideltagerListe();	//lager templiste som f�r deltagerlisten
			if (!templist->inList(*nr))						//Hvis nr ikke finnes i listen
			{
				Deltager* deltager = new Deltager(nr);		//Oppretter ny deltager med nr
				templist->add(deltager);					//Legger til deltager i listen
				deltagere.tellDeltager();					//Plusser p� antall deltager
			}
			else											//Hvis nummeret finnes skrives feilmelding
				cout << "Dette nummeret er allerede i bruk";
		}

		///////////////////////Endre deltager///////////////////////
		void endreDeltager()
		{
			bool iListe = false;							//Initialiserer bool til false
			List* tempList = deltagere.gideltagerListe();	//oppretter templist som f�r deltagerlisten
			int delnr;
				cout << "Skriv inn deltagerens nummer: ";	//Sp�r om deltagerns nummer
				cin >> delnr;								//Leser deltagerns nummer
				iListe = tempList->inList(delnr);			//sjekker om medsendt nummer finnes
				if (iListe == false)						//Hvis nummer ikke finnes
				{
					cout << "\nUgyldig deltagernummer\n";		//Feilmelding
					system("pause");
					skrivDeltagerMeny();
				}
				else										//Hvis  nummer finnes
				{
					Deltager * tempDeltager = (Deltager*)tempList->remove(delnr);	//Fjerner gitt deltager fra liste
					tempDeltager->endreDeltager(tempDeltager);						//Kj�rer endre nasjon
					tempList->add(tempDeltager);									//Legger tilbake i listen
					system("pause");
					skrivGrenMeny();
				}
		}

		////////////////////////////Skriv Deltager/////////////////////////
		void skrivDeltagere()
		{
			char * temp = new char[20];						//char pointer, 20 lang
			bool iListe = false;							//Initialiserer bool Iliste
			List * tempList = nasjoner.giNasjonListe();		//lager temp liste som blir kj�rt ginasjonsliste p�
			cout << "Skriv forkortelse: ";				//Sp�r om forkortelse
			cin >> temp;								//Leser forkortelse
			iListe = tempList->inList(temp);			//Sjekker om forkortelse er i listen
			if (iListe == false)						//Hvis ikke
			{
				cout << "Ugyldig forkortelse.\n";		//Skirver feilmelding, pause og relevant nasjonmeny
				system("pause");
				skrivNasjonMeny();
			}
			else
			{
				skrivTropp(temp);								//Skrivtropp blir kj�rt p� forkortelsen
				system("pause");
				skrivNasjonMeny();
			}
		}

		/////////////////////Skriv en deltager////////////////////////
		void skrivEnDeltager()
		{
			int deltagernr;
			List * tempList = deltagere.gideltagerListe();			//Oppretter templist som f�r deltagerlisten
				bool finnes = false;
					cout << "Deltagerens nr: ";						//Sp�r om deltagerns nummer
					deltagernr = sjekkInt();						//Leser deltagerns nummer
					finnes = tempList->inList(deltagernr);			//sjekker om deltagernummer finnes
					if (finnes == false)							//Hvis det ikke finnes
					{
						cout << "\nUgyldig deltagernummer\n";		//Feilmelding, pause, relevant meny
						system("pause");
						skrivDeltagerMeny();
					}
					else											//Hvis nummer finnes
					{
						Deltager* deltager = (Deltager*)tempList->remove(deltagernr);	//Fjerner gitt deltager fra listen
						deltager->display();						//Kj�rer display funksjon
						tempList->add(deltager);					//Legger deltager tilbake til listen
						system("pause");							//Pauser
						skrivDeltagerMeny();						//skriver deltager meny
					}
		}

		///////////////////////Skriv tropp////////////////////////
		void skrivTropp(char * forkortelse)
		{
			char * temp;
			List * tempList = deltagere.gideltagerListe();			//Lager templiste som f�r deltagerlisten
			int antDeltagere = deltagere.giAntDeltagere();
			for (int i = 1; i <= antDeltagere; i++)
			{
				Deltager * tempDeltager = (Deltager*)tempList->removeNo(i);		//Fjerner gitt nummer fra listen
				if (tempDeltager != NULL)							//Hvis deltagerpointeren ikke er tom
				tempDeltager->omTropp(forkortelse);					//skriver ut gitt deltagers informasjon
				tempList->add(tempDeltager);						//Legger tilbake deltager i liste
			}
		}
		
		//////////////////Hent deltagerliste//////////////////
		void hentDeltagerListe(List temp)
		{
			deltagere.gideltagerListe();	//kj�rer gideltagerliste p� gitt deltagere-objekt
		}
		//////////////////Skriv deltagerliste//////////////////
		void skrivDeltagerListe()
		{
			deltagere.display();			//Kj�rer deltagere sin display funksjon
			cout << endl;
			system("pause");
			skrivDeltagerMeny();
		}
	
		//////////////////////////////////////////////////GREN////////////////////////////////////////////////

		/////////////////////////////NyGren//////////////////////
		void nyGren()
		{
			string* grenNavn = new string;				//Ny grennavn pointer
			cout << "Grenens Navn: ";					//Sp�r om grenens navn
			getline(cin, *grenNavn);					//Leser grenens navn
			const char* grenNvn = grenNavn->c_str();	//gj�r grenavn om til char
			List * templist = grener.giGrenListe();		//temp liste f�r grenlisten
			if (!templist->inList(grenNvn))				//Hvis grenavnet ikke finnes
			{
				Gren* gren = new Gren(grenNvn);			//Oppretter ny gren ved grenavnet
				templist->add(gren);					//legger til gren i liste
				grener.tellGren();						//Plusser p� antall gren
				skrivGrenMeny();						//Skriver gren meny
			}
			else										//Hvis grennavnet finnes, kommer feilmelding, kj�res p� nytt
			{
				cout << "Dette Gren-navnet finnes allerede\n";
				nyGren();
			}
		}

		///////////////////////Endre Gren/////////////////////
		void endreGren()
		{
			bool iListe = false;						//Initialiserer bool til false
			List * tempList = grener.giGrenListe();		//oppretter templiste som f�r grenlisten
			char * nvn = new char[20];					//oppretter charpointer 20 lang
				cout << "Skriv inn grennavn: ";			//sp�r om grenavn
				cin >> nvn;								//Leser grennavn
				iListe = tempList->inList(nvn);			//Setter iListe lik om navnet allerede finnes i listen
				if (iListe == false)					//Hvis navnet ikke finnes
				{
					cout << "Ugyldig gren.\n";			//Feilmelding
					system("pause");
					skrivGrenMeny();
				}
				else									//Hvis navnet finnes
				{
					Gren * tempGren = (Gren*)tempList->remove(nvn);			//Fjerner objektet med navnet fra listen
					tempGren->endreGren(tempGren);		//Kj�rer endre nasjon
					tempList->add(tempGren);			//Legger tilbake i listen
					system("pause");
					skrivGrenMeny();
				}
		}

		////////////////////Hent grenliste/////////////////
		void hentGrenListe(List temp)
		{
			grener.giGrenListe();			//f�r parameter, returnerer grenliste
		}

		/////////////////Skriv grenliste//////////////////
		void skrivGrenListe()
		{
			grener.display();				//Kj�rer displayfunksjon
			cout << endl;
			system("pause");
			skrivGrenMeny();
		}

		//////////////////////Endre gren//////////////////
		void skrivEnGren()
		{
			char * grenNvn = new char;					//ny navn pointer
			List * tempList = grener.giGrenListe();		//oppretter templist som f�r liste
			bool finnes = false;
				cout << "Gi grennavn: ";				//sp�r om grennavn
				cin >> grenNvn;							//Leser grennavn
				finnes = tempList->inList(grenNvn);		//sjekker om grennavn finnes i listen
				if (finnes == false)					//Hvis det ikke finnes
				{
					cout << "Ugyldig gren.\n";			//Feilmelding
					system("pause");
					skrivGrenMeny();
				}else									//Hvis navnet finnes
				{
					Gren * gren = (Gren*)tempList->remove(grenNvn);		//Fjerner gitt gren fra listen
					gren->display();					//Kj�rer displayfunksjon p� gitt gren
					tempList->add(gren);				//Legger gitt gren tilbake i listen
					system("pause");
					skrivGrenMeny();
				}
			}

		///////////////////Finnes gren///////////////////
		bool finnesGren(char * gren)
		{
			List* temp = grener.giGrenListe();			//Lager templiste som mottar liste
			if (temp->inList(gren))						//Hvis navnparameteren er i listen
			{
				return true;							//Returnerer true
			}
			else										//Hvis navnparameteren ikke finnes i listen
				cout << "\nGrenen finnes ikke!\n";		//Feilmelding, og returenerer false
			return false;
		}

		///////////////////Antgren////////////////
		int antGren()
		{
			int tempgren = grener.giAnt();				//Returnerer antall grener
			return tempgren;
		}
		
		///////////////////////Skriv gren til fil////////////////////
		void skrivGrenTilFil()
		{
			grener.skrivGrenFil();						//Kj�rer skrivgrenfil
		}

		/////////////////////////////////////////////�VELSER/////////////////////////////////////////////////

		/////////////////////Ny �velse/////////////////////
		void nyOvelse(char * grenNvn)
		{
			string * ovelseNavn = new string;	//Lagring for ovelsenavn
			List * tempList = grener.giGrenListe();					//Oppretter templiste som f�r grenliste
			Gren * tempGren = (Gren*)tempList->remove(grenNvn);		//Fjerner listen som har blitt lest inn f�r
			bool finnesOvelse = false;	//Om ovelsenavn finnes fra f�r
			int * tempNr = tempGren->giAntOvelser();				//oppretter temp pointer som f�r antall �velser
			if (*tempNr < MAXOVELSER)								//Hvis tempnummer er tom
			{
				cout << "\n�velsens Navn: ";							//sp�r om �velsenavn
				getline(cin, *ovelseNavn);									//Leser �velsenavn
				finnesOvelse = tempGren->finnesOvelse(ovelseNavn);
				if (finnesOvelse == false)
				{
					if (tempNr == NULL)									//Hvis tempnummer er tom
					{
						tempNr = new int;								//oppretter ny int og initialiserer til 0
						*tempNr = 0;
					}
					Ovelse * tempOvelser = new Ovelse(ovelseNavn, tempNr);	//oppretter ny �velse med navn og nr
					tempGren->leggTilIArray(tempOvelser);					//Legger �velse i array
				}
				else
					cout << "\nOvelse med samme navn finnes allerede.\n";
			}
			else
				cout << "\nDet er ikke plass til flere ovelser i grenen.\n";
			tempList->add(tempGren);	//Legger grenen tilbake i listen
			system("pause");	//Gir bruker tid til � lese feilmeldinger
			skrivOvelseMeny();	//Skriver forrige meny
		}

		///////////////////Skriv alle �velser/////////////////////
		void skrivAlleOvelser(char * nvn)
		{
			List * tempList = grener.giGrenListe();			//oppretter templist som f�r gren-listen
			Gren * tempGren = (Gren*)tempList->remove(nvn);	//tempgren med allerede valgt navn, blir fjernet fra listen
			
			tempGren->display();							//Kj�rer display funksjon
			tempList->add(tempGren);						//Legger tilbake tempgren i listen
			system("pause");	//Gir bruker tid til � lese utdata
			skrivOvelseMeny();	//Skriver forrige meny
		}

		/////////////////Endre �velser/////////////////////
		void endreOvelse(char * grenNvn)
		{
			int tempI = 25;
			bool navnSjekk = false;
			List * tempList = grener.giGrenListe();				//templiste som f�r grenlisten
			Gren * tempGren = (Gren*)tempList->remove(grenNvn);	//fjerner allerede gitt gren fra listen
			int * antOvelser = tempGren->giAntOvelser();		//ant�vlser pointer f�r antall �velser
			if (*antOvelser != 0)								//hvis antall �velser ikke er 0
			{
				cout << "\nOvelsens navn: \n";					//Sp�r om navn
				string * tempOvelseNavn = new string;				//oppretter pointer
				getline(cin, *tempOvelseNavn);						//Leser navn
				for (int i = 1; i <= *antOvelser; i++)				//Looper helt til antall �velser
				{
					Ovelse * tempOvelser = tempGren->giOvelse(i);	//returnerer arrayen i �velsen plass
					navnSjekk = tempOvelser->sjekkNavn(tempOvelseNavn);		//Sjekker om navn finnes
					if (navnSjekk == true)							//Hvis navn finnes
						tempI = i;
				}
				if (tempI != 25)								//Hvis navn ikke er 25
				{
					Ovelse * tempOvelser = tempGren->giOvelse(tempI);	//returnerer �velse i arrayens plass
					tempOvelser->endreOvelse(tempOvelser);		//Kj�rer endre �velsefunksjon
				}
				else
					cout << "Finner ikke ovelse.\n";			//Skriver feilmelding
				tempList->add(tempGren);						//Legger tilbake i listen
			}
			else
			cout << "\nIngen ovelser registrert\n";				//skriver feilmelding
			system("pause");	//Gir bruker tid til � lese feilmelding
			skrivOvelseMeny();	//Skriver forrige meny
		}

		////////////////////Finnes �velse///////////////////
		bool finnesOvelse(int * ovelseNr, char * grenNvn)
		{
			List * tempList = grener.giGrenListe();					//templist f�r grenliste
			Gren * tempGren = (Gren*)tempList->remove(grenNvn);		//fjerner gren med gitt navn fra listen
			bool eksistens = false;
			int * antOvelser = tempGren->giAntOvelser();			//int pointer f�r antall �velser
			int * tempNr = new int;
			for (int i = 1; i <= *antOvelser; i++)					//Kj�rer helt til antall �velser
			{
				Ovelse * tempOvelse = tempGren->giOvelse(i);		//returnerer �velse i arrayens plass
				*tempNr = tempOvelse->giOvelseNr();					//setter tempnr lik �velsens nr
				tempList->add(tempGren);							//Legger gren tilbake i liste
				if (*tempNr == *ovelseNr)							//Hvis tempnr matcher
					eksistens = true;
			}
			tempList->add(tempGren);
			if (eksistens == true)	//Returnerer true dersom ovelse finnes
				return true;
		}
		/////////////////////////////////////////Fjerner en ovelse ifra ovelsearrayen i gren///////////////////////////
		void fjernOvelse(char * grenNvn, int * ovelseNr) {
			List * tempList = grener.giGrenListe();	//Henter ut listen over grener
			Gren * tempGren = (Gren*)tempList->remove(grenNvn);	//Henter ut grenen som vi opererer i
			int * antOvelser = tempGren->giAntOvelser();	//Henter ut antall ovelser
			int * tempNr = new int;	//Lagring for ovelse nr bruker vil fjerne
			for (int i = 1; i <= *antOvelser; i++) {	//Looper igjennom alle ovelser
				Ovelse * tempOvelse = tempGren->giOvelse(i);	//Henter ut ovelse i
				*tempNr = tempOvelse->giOvelseNr();	//Henter ut nr p� ovelse i
				if (*tempNr == *ovelseNr)	//Dersom ovelse i har nr gitt av bruker
					tempOvelse = NULL;	//Nullstiller ovelsen bruker ba om
			}

			tempGren->reduserAntOvelser();	//Registrerer en ovelse mindre i array
			tempList->add(tempGren);	//Legger grenen tilbake i listen
			cout << "\nOvelse fjernet!\n";
			system("pause"); 
			skrivOvelseMeny();
		}

		int giAntOvelser(char * grenNvn)
		{
			List * tempList = grener.giGrenListe();
			Gren * tempGren = (Gren*)tempList->remove(grenNvn);
			int * antGrener = tempGren->giAntOvelser();
			tempList->add(tempGren);
			return *antGrener;
		}

		//////////////////////////////////////////////POENG//////////////////////////////////////////////////
		//////////////////Regner ut totalt antall medaljer og poeng for hver nasjon///////////////////
		void kalkulerPoengOgMedaljer(ifstream & poengFil, int * antIListe) {
			char * innData = new char;	//Lagring for data fra fil
			int * poengHolder = new int;	//Lagring for plassering
			string * soppelDunk = new string;	//S�ppeldunk for unyttig info p� fil
			int * poengCounter[MAXNASJONER] = { NULL };	//Lagring for poeng for hver nasjon
			int * gullCounter[MAXNASJONER] = { NULL };	//Lagring for gull for hver nasjon
			int * solvCounter[MAXNASJONER] = { NULL };	//Lagring for s�lv til hver nasjon
			int * bronsjeCounter[MAXNASJONER] = { NULL };	//Lagring for bronsje for hver nasjon
			char * nasjonsOversikt[MAXNASJONER] = { NULL };	//Oversikt over alle forkortelser
			for (int i = 1; i <= *antIListe; i++)	//Looper igjennom hele listen
			{
				poengFil >> innData;	//Henter ut s�ppel
				poengFil.ignore();	//Ignorerer linjeskift
				getline(poengFil, *soppelDunk);	//Henter ut linje med s�ppel
				poengFil >> innData;	//Henter ut s�ppel
				poengFil.ignore();	//Ignorerer linjeskift
				bool utfort = false;	//Regneoperasjon er utf�rt
				for (int y = 1; utfort != true; y++)	//Looper til regneoperasjonen er gjort
				{
					if (nasjonsOversikt[y] == NULL)	//Dersom ingen ting er lagt i skuffen
					{
						nasjonsOversikt[y] = new char;	//Lagring for forkortelse
						memcpy(nasjonsOversikt[y], innData, strlen(innData) + 1);	//Kopierer forkortelse til skuff i array
						poengCounter[y] = new int;	//Lagring for poeng
						gullCounter[y] = new int;	//Lagring for gull
						solvCounter[y] = new int;	//Lagring for s�lv
						bronsjeCounter[y] = new int;	//Lagring for bronsje
						*poengCounter[y] = 0;	//Initialiserer poeng til 0
						*gullCounter[y] = 0;	//Initialiserer gull til 0
						*solvCounter[y] = 0;	//Initialiserer s�lv til 0
						*bronsjeCounter[y] = 0;	//Initialiserer bronsje til 0
						poengFil >> *poengHolder;	//Henter ut plassering
						switch (*poengHolder)	//Switcher p� plassering
						{
						case 1:	//Ved forsteplass
							*poengCounter[y] += 7;	//Legger til 7 i poeng
							*gullCounter[y] += 1;	//Registrerer en ekstra gull
							break;	//Case ferdig
						case 2:	//Deltager Meny
							*poengCounter[y] += 5;	//Legger til 5 i poeng
							*solvCounter[y] += 1;	//Legger til 1 i s�lv
							break;	//Case ferdig
						case 3:	//Grener Meny
							*poengCounter[y] += 3;	//Legger til 3 i poeng
							*bronsjeCounter[y] += 1;	//Legger til 1 i bronsje
							break;	//Case ferdig
						case 4:
							*poengCounter[y] += 2;	//Legger til 2 i poeng
							break;	//Case ferdig
						case 5:
							*poengCounter[y] += 1;	//Legger til 1 i poeng
							break;	//Case ferdig
						}
						poengFil.ignore();	//Ignorerer linjeskift
						utfort = true;	//Regneoperasjon er utf�rt og l�kken kan brytes
					}
					else if (nasjonsOversikt[y] != NULL && *nasjonsOversikt[y] == *innData)	//Dersom skuffen har innhold og det stemmer med forkortelse under behandling
					{
						poengFil >> *poengHolder;	//Henter ut plassering
						switch (*poengHolder)	//Switcher p� plassering
						{
						case 1:	//Ved forsteplass
							*gullCounter[y] += 1;	//Legger til en gullmedaljer
							*poengCounter[y] += 7;	//Legger til 7 poeng
							break;		//Case ferdig
						case 2:	//Deltager Meny
							*solvCounter[y] += 1;	//Legger til en s�lvmedaljer
							*poengCounter[y] += 5;	//Legger til 5 poeng
							break;	//Case ferdig
						case 3:	//Grener Meny
							*bronsjeCounter[y] = 1;	//Legger til en bronsje
							*poengCounter[y] += 3;	//Legger til 3 poeng
							break;	//Case ferdig
						case 4:	//�velser Meny
							*poengCounter[y] += 2;	//Legger til 2 poeng
							break;	//Case ferdig
						case 5:
							*poengCounter[y] += 1;	//Legger til 1 poeng
							break;	//Case ferdig
						}
						poengFil.ignore();	//Ignorerer linjeskift
						utfort = true;	//Regneoperasjon er utf�rt og l�kke kan brytes
					}
				}
			}
			for (int i = 1; i <= *antIListe; i++)	//G�r igjennom alle forkortelser og registrerer poeng og medaljer p� disse
			{
				if (poengCounter[i] != NULL)	//Dersom det finnes innhold
				{
					poenger.sendPoengerMedNasjoner(nasjonsOversikt[i], poengCounter[i]);	//Registrerer utregnede poeng i poengklassen
					medaljer.sendMedaljerMedNasjoner(nasjonsOversikt[i], gullCounter[i], solvCounter[i], bronsjeCounter[i]);	//Registrerer utregnede medaljer i medaljeklassen
				}
			}
			}
	
////////////////////G�r igjennom alle grener og �velser sine .RES filer og sender .RES videre//////////////
		void hentMedaljerOgPoeng()
		{
			List * tempList = grener.giGrenListe();	//Henter ut listen med grener
			int antallGrener = grener.giAnt();	//Henter ut antall grener
			int * antIListe = new int;	//Antall deltagere i liste
			int aktuellOvelse = 1;	//�velse som er under behandling
			for (int i = 1; i <= antallGrener; i++)	//G�r igjennom alle grener
			{
				Gren * tempGren = (Gren*)tempList->removeNo(i);	//Henter ut gren
				int * antallOvelser = tempGren->giAntOvelser();	//Henter ut antall ovelser i gren
				char * grenNvn = tempGren->giGrenNavn();	//Henter ut grennavnet
				for (int i = 1; i <= *antallOvelser; i++)	//G�r igjennom alle �velser
				{
					string filNavnRES = grenNvn + to_string(i) + ".RES";	//Genererer filnavn
					ifstream poengFil(filNavnRES);	//Fors�ker �pne fil
					if (poengFil)	//Dersom fil ble �pnet
					{
						poengFil >> *antIListe;	//Henter ut antall i listen fra fil
						poengFil.ignore();	//Ignorerer linjeskift
						kalkulerPoengOgMedaljer(poengFil, antIListe);	//Regner ut antall poeng+medaljer og sender dette til klassene
					}
					aktuellOvelse++;	//Adderer hvilken ovelse som er under behandling
				}
				tempList->add(tempGren);	//Legger gren tilbake i liste
			}
		}
//////////////////////////////////////POENG//////////////////////////////////////////
		void skrivPoeng()
		{
			int antNasjon = nasjoner.giAnt();	//Henter ut antall nasjoner registrert
			for (int i = 1; i <= antNasjon; i++)	//Looper igjennom antall mulig forkortelser
			{
				char * forkort = poenger.giTilgang(i);	//Henter ut forkortelsen
				int * temp = poenger.giSkuffNr(forkort);	//Henter ut skuffNr til forkortelsen
				if (forkort != NULL)	//Dersom forkortelsen ikke er nllptr
				{
					poenger.displayForkort(forkort);	//Displayer forkortelsen
					poenger.displayPoeng(*temp);	//Displayer poeng for forkortelsen
				}
			}
			cout << "\n";
			system("pause");
			skrivHovedMeny();
		}
		////////////////////////////////////////////////////////MEDALJER/////////////////////////////////////
		////////////////////////////////////Skriver ut antall medaljer for hver nasjon////////////////////////
		void skrivMedaljer()
		{
			int antNasjon = nasjoner.giAnt();	//Henter ut totalt mulig registrerte forkortelser
			for (int i = 1; i <= antNasjon; i++)	//G�r igjennom alle forkortelser og skriver ut poeng for disse
			{
				char * forkort = medaljer.giTilgang(i);	//Henter ut forkortelse fra array med disse
				int * temp = medaljer.giSkuffNr(forkort);	//Henter ut skuffNr for forkortelsen
				if (forkort != NULL)	//S� lenge forkortelsen ikke er en nllptr
				{
					medaljer.displayForkort(forkort);	//Displayer forkortelsen
					medaljer.display(temp);	//Displayer medaljene som h�rer forkortelsen til
				}
			}
			cout << "\n";
			system("pause");
			skrivHovedMeny();
		}
		///////////Nullstiller medaljer og poeng//////////////
		void nullStill() 
		{
			medaljer.nullStillMedaljer();	//Nullstiller medaljer
			poenger.nullStillPoeng();	//Nullstiller poeng
		}

		///////////////////////////////////////////////RESULTATER/////////////////////////////////////
//////////////////////////////////////////////////////Oppretter ny resultatliste med info fra startliste///////////////////
		void nyResultatListe(char * grenNvn, int * ovelseNr)
		{
			int * i = new int;	//Antall deltagere
			int * counter = new int;	//Holder styr p� hvilken deltager som behandles
			*counter = 0;	//Initiliserer counter til 0
			string * dump = new string;	//S�ppeldunk for urelevant info i fil
			int * tempArray[MAXDELTAGER];	//Inneholder deltakere sine unike nr
			string filNavnRES = grenNvn + to_string(*ovelseNr) + ".RES";	//Genererer filnavn for resultatfil
			string filNavnSTA = grenNvn + to_string(*ovelseNr) + ".STA";	//Genererer filnavn for deltagerfil
			ofstream utfil(filNavnRES);	//Fors�ker �pne resultatfil for skriving
			int * ID = new int;	//Lagring for unikt deltagernummer
			int * plass = new int;	//Lagring for plassering
			bool eksistens = false;	//Om deltager eksisterer
			List * tempList = deltagere.gideltagerListe();	//Henter ut deltagerlisten
			Deltager * tempDeltager;	//Portal til info om deltager
			ifstream innFil(filNavnSTA);	//Fors�ker �pne startlisten for lesing
			if (innFil) {	//Dersom fil ble �pnet
				innFil >> *i;	//Henter ut antall deltagere fra deltagerlisten
				utfil << *i << endl;	//Skriver antall deltagere i �velse til resultat
				for (int counter = 0; counter < *i; counter++)	//G�r igjennom alle deltagere i deltagerlisten og henter ut unike nr
				{
					tempArray[counter] = new int;	//Lagring for deltagerNr
					innFil >> *tempArray[counter];	//Henter ut deltagerNr fra deltagerlisten
					innFil.ignore();	//Ignorerer linjeskift
					getline(innFil, *dump);	//Henter ut s�ppel
					getline(innFil, *dump);	//Henter ut s�ppel
				}
				while (*counter < *i) {	//S� lenger deltaker under behandling er lavere enn totalt antall
					*ID = *tempArray[*counter];	//Henter ut deltakers nr
					eksistens = tempList->inList(*ID);	//Sjekker at deltaker finnes
					if (eksistens == true) {	//Dersom deltaker eksisterer
						cout << "\nDeltager Nr: " << *tempArray[*counter] << "'s " << "Plassering: ";
						cin >> *plass;	//Tar imot plassering i ovelsen
						tempDeltager = (Deltager*)tempList->remove(*ID);	//Henter ut deltageren
						tempDeltager->deltagerTilResultatListe(utfil, tempDeltager, plass);	//Skriver deltageren til resultatlisten
						tempList->add(tempDeltager);	//Legger deltageren tilbake til listen
						++(*counter);	//�ker oversikt over hvilken deltager som behandles
					}
				}
				utfil.close();	//Stenger utfilen
				system("pause");
				skrivResultatListeMeny();	//Skriver forrige meny
			}
		}
///////////////////////////////////Skriver ut alt innhold i resultatlisten//////////////////////
		void skrivResultatListe(char * grenNvn, int * ovelseNr) {
			string filNavnRES = grenNvn + to_string(*ovelseNr) + ".RES";	//Generer filnavn
			ifstream innfil(filNavnRES);	//Fors�ker �pne filen
			string * output = new string;	//Lagring for data fra fil
			string * Dump = new string;		//Soppeldunk for unyttig data
			int antDel;	//Antall deltagere
			if (innfil) {	//Dersom filen ble �pnet
				innfil >> antDel;	//Henter ut antall deltagere fra fil
				innfil.ignore();	//Ignorerer linjeskift
				cout << "Antall deltagere: "<< antDel << "\n\n";
				for (int i = 1; i <= antDel; i++) {	//Skriver ut alle deltagere p� fil
					getline(innfil, *Dump);	//Henter nummer
					cout << "Deltager  Nummer: " << *Dump << endl;
					getline(innfil, *Dump);	//Henter navn
					cout << "Navn: " << *Dump << endl;
					getline(innfil, *Dump);	//Henter nasjon
					cout << "Nasjon: " << *Dump << endl;
					getline(innfil, *Dump);	//Henter plassering
					cout << "Plass: " << *Dump << "\n\n";
			
				}
				system("pause");
				skrivResultatListeMeny();	//Skriver forrige meny
			}
			else cout << "\nIngen liste er opprettet.\n";	//Dersom liste ikke ble �pnet
			system("pause");
			skrivResultatListeMeny();	//Skriver forrige meny
		}
////////////////////////////////Sletter resultatlisten//////////////////////
		void fjernResultatListe(char * grenNvn, int * ovelseNr)
		{
			string filNavnRES = grenNvn + to_string(*ovelseNr) + ".RES";	//Genererer filnavn
			remove(filNavnRES.c_str());	//Sletter filen
			cout << "\nFil slettet.\n";
			system("pause");
			skrivResultatListeMeny();	//Skriver forrige meny
		}
			
		///////////////////////////////////////////////////LISTER/////////////////////////////////
		////////////////////Oppretter en ny deltagerliste//////////////////
		void nyDeltagerListe(char * grenNvn, int * ovelseNr)
		{
			int * deltagerArray[MAXDELTAGER]{ NULL };	//Oppretter en int array for mellomlagring av deltakernummere
			int * counter = new int;	//Mellomlagring for antall deltagere i liste
			*counter = 0;	//Initiliserer antall deltagere til 0
			string filNavnSTA = grenNvn + to_string(*ovelseNr) + ".STA";	//Oppretter filnavnet for listen
			ifstream filSjekk(filNavnSTA);
			if (!filSjekk)
			{
				filSjekk.close();
				ofstream utfil(filNavnSTA);	//Oppretter nytt utfil objekt
				if (utfil)	//Dersom fil ble opnet
				{
					char * input = new char;	//Mellomlagring for bruker input
					int * ID = new int;	//Mellomlagring for deltakernummer
					bool eksistens = false;	//Mellomlagring for hvorvidt en deltaker eksisterer
					List * tempList = deltagere.gideltagerListe();	//Henter ut listen med deltagere
					Deltager * tempDeltager;	//Midlertidig lagring for deltager
					do {
						cout << "\nQ for � lagre, eller N for ny deltager: \n";
						*input = lesKommando();	//Tar imot input fra bruker
						if (*input == 'N') {	//Dersom bruker gir N
							cout << "\nDeltagerNR: ";
							*ID = sjekkInt();	//Tar imot et deltakernummer fra bruker
							eksistens = tempList->inList(*ID);	//Returnerer hvorvidt deltageren eksisterer



							bool altIListe = false;
							for (int i = 0; i <= MAXDELTAGER; i++)	//Looper igjennom alle deltagerne og henter ut de unike nr
							{
								if (deltagerArray[i] == NULL)
									i = MAXDELTAGER;
								else if (*deltagerArray[i] == *ID)
									altIListe = true;
							}

							if (eksistens == true && altIListe == false) {	//Dersom deltager eksisterer
								deltagerArray[*counter] = new int;
								*deltagerArray[*counter] = *ID;	//Lagrer brukerens deltakernummer i f�rste ledige skuff
								++(*counter);	//�ker antall deltakere telleren
							}
							else if (eksistens != true)
								cout << "\nNR er ikke registrert.\n";

							if (altIListe == true)
								cout << "\nDeltager er alt i listen.\n";
						}
					} while (*input != 'Q');
					utfil << *counter << endl;	//Skriver antall deltagere til liste
					for (int i = 0; i < *counter; i++)	//Legger til alle deltagerne til deltagerlisten
					{
						tempDeltager = (Deltager*)tempList->remove(*deltagerArray[i]);	//Henter ut deltager
						tempDeltager->deltagerTilDeltagerListe(utfil, tempDeltager);	//Skriver deltager til deltagerliste
						tempList->add(tempDeltager);	//Legger deltager tilbake til liste
					}
					utfil.close();	//Stenger utfilen
				}
			}
			else
				cout << "\nFil finnes allerede.\n";
			system("pause");
			skrivStartListeMeny();
		}
/////////////////////////////////////////Skriver ut alt innhold p� deltagerlisten//////////////////
		void skrivDeltagerStartListe(char * grenNvn, int * ovelseNr)
		{
			int * counter = new int;	//Antall deltagere
			string filNavnSTA = grenNvn + to_string(*ovelseNr) + ".STA";	//Genererer filnavn
			ifstream utfil(filNavnSTA);	//Fors�ker �pne deltagerlisten
			string * output = new string;	//Lagring for innhold i liste
			if (utfil) {	//Dersom fil ble �pnet
				utfil >> *counter;	//Henter ut antall deltagere fra fil
				cout << "Antall deltagere: " << *counter << endl;
				utfil.ignore();	//Ignorerer linjeskift
				while (getline(utfil, *output))	//Henter ut og skriver til skjerm all innhold i fil
				{
					cout << *output << endl;
				}
				system("pause");
				skrivStartListeMeny();
			}
			else 
			{
				cout << "\nIngen liste er opprettet.\n";	//Dersom fil ikke ble �pnet
				system("pause");
				skrivStartListeMeny();
			}
		}
////////////////////////////////////Sletter deltagerliste filen////////////////////////
		void fjernDeltagerListe(char * grenNvn, int * ovelseNr)
		{
			string filNavnSTA = grenNvn + to_string(*ovelseNr) + ".STA";	//Genererer filnavn
			remove(filNavnSTA.c_str());	//Sletter fil med gitt navn
			cout << "\nFil " << filNavnSTA <<" slettet.\n";
			system("pause");
			skrivStartListeMeny();	//Skriver forrige meny
		}
////////////////////////////////////////Endre en deltagerliste for ovelse/////////////////////
		void endreDeltagerListe(char * grenNvn, int * ovelseNr)
		{
			int * antIListe = new int;	//Antall deltagere i listen
			string * dump = new string;	//S�ppeldunk for uvesentlig info p� fil
			int * tempArray[MAXDELTAGER];	//Deltagernummere i array
			string filNavnSTA = grenNvn + to_string(*ovelseNr) + ".STA";	//Generer filnavn p� deltagerlisten
			ifstream innFil(filNavnSTA);	//Fors�ker �pne deltagerlisten for lesing
			if (innFil) {	//Dersom �pnet
				innFil >> *antIListe;	//Antall deltagere i listen hentes ut
				for (int counter = 0; counter < *antIListe; counter++)	//Looper igjennom alle deltagerne og henter ut de unike nr
				{
					tempArray[counter] = new int;
					innFil >> *tempArray[counter];	//Henter ut deltagernummer til array
					innFil.ignore();	//Ignorerer linjeskift
					getline(innFil, *dump);	//Henter ut s�ppel
					getline(innFil, *dump);	//Henter ut s�ppel
				}
				char * kommando = new char;	//Input fra bruker
				List * tempList = deltagere.gideltagerListe();	//Henter ut liste over alle registrerte deltagere
				Deltager * tempDeltager;	//Portal til all info om en deltager
				innFil.close();	//Stenger deltagerlisten
				ofstream utFil(filNavnSTA);	//Fors�ker �pne deltagerlisten for skriving
				while (*kommando != 'F' && *kommando != 'L' && *kommando != 'Q') {	//S� lenge ugyldig kommando
					cout << "(F)jern/(L)egg til deltager. ((Q)uit." << endl;
					*kommando = lesKommando();	//Tar imot input fra bruker
					switch (*kommando)	//Switcher p� kommando
					{
					case 'F': {	//Fjern deltager
						cout << "\nNR: ";
						int nr = sjekkInt();	//Tar imot unikt deltagerNR fra bruker
						int tempAnt = *antIListe;	//Antall deltagere i listen
						for (int p = 0; p < *antIListe; p++)	//G�r igjennom alle deltagerne i listen
						{
							if (*tempArray[p] == nr) {	//Dersom gitt nummer stemmer med innhold i array
								tempArray[p] = NULL;	//Array posisjonen nulles
								--(tempAnt);	//Reduserer n�v�rende antall i listen
							}
						}
						utFil << tempAnt << endl;	//Skriver nytt antall til listen
						for (int p = 0; p < *antIListe; p++) {	//G�r igjennom og legger til alle deltagerne i listen
							if (tempArray[p] != NULL)	//S� lenge posisjonen ikke er nullptr
							{
								tempDeltager = (Deltager*)tempList->remove(*tempArray[p]);	//Henter ut rett deltaker vha unikt nr i array
								tempDeltager->deltagerTilDeltagerListe(utFil, tempDeltager);	//Skriver deltageren til listen
								tempList->add(tempDeltager);	//Legger deltager tilbake i listen
							}
						}
					}
							  break;	//Case ferdig
					case 'L':	//Legger til en ny deltager i lsiten
					{
						cout << "\nNR: ";
						int nr = sjekkInt();	//Mottar unikt nr fra bruker
						int tempAnt = *antIListe;	//Henter ut antall i liste
						bool sjekk = false;	//Om allerede er i liste
						for (int p = 0; p < *antIListe; p++)	//G�r igjennom listen og sjekker om alt der
						{
							{
								if (*tempArray[p] == nr) {	//Dersom nr er likt gitt nr
									cout << "\nFinnes allerede i liste.\n";
									sjekk = true;	//Finnes alt i liste
								}
							}
						}
						
						if (sjekk != true)	//Dersom ikke i liste
						{
							if (tempList->inList(nr)) {	//Sjekker at nr faktisk er registrert
								utFil << ++tempAnt << endl;	//Skriver antall +1 til fil
								tempDeltager = (Deltager*)tempList->remove(nr);	//Henter ut reel deltager
								if (tempDeltager != NULL) {	//S� lenge ikke nullptr
									tempDeltager->deltagerTilDeltagerListe(utFil, tempDeltager);	//Skriver rett deltager til deltagerliste
									tempList->add(tempDeltager);	//Legger deltager tilbake i liste
								}
								for (int p = 0; p < *antIListe; p++)	//Skriver deltagerne som alt fantes tilbake til deltagerliste
								{
									tempDeltager = (Deltager*)tempList->remove(*tempArray[p]);	//Henter ut tidligere registrert deltager
									if (tempDeltager != NULL) {	//S� lenge deltageren er hentet ut riktig
										tempDeltager->deltagerTilDeltagerListe(utFil, tempDeltager);	//Skriver deltageren til deltagerlisten
										tempList->add(tempDeltager);	//Legger deltageren tilbake i listen
									}
								}
							}
						}
						else {	//Dersom ny deltager ikke ble lagt til
							if (!tempList->inList(nr))	//Informerer dersom nummeret ikke er registrert
							cout << "\nNummer er ikke registrert.\n";
							utFil << tempAnt << endl;	//Skriver antall tilbake til liste
							for (int p = 0; p < *antIListe; p++)	//Legger opprinnelige deltagere tilbake i listen
							{
								tempDeltager = (Deltager*)tempList->remove(*tempArray[p]);	//Henter ut deltager
								if (tempDeltager != NULL) {	//Dersom ikke nullptr
									tempDeltager->deltagerTilDeltagerListe(utFil, tempDeltager);	//Skriver deltager til deltagerliste
									tempList->add(tempDeltager);	//Legger deltageren tilbake i listen
								}
							}
						}
						utFil.close();	//Stenger utfilen
						break; }	//Case ferdig
					case 'Q':	//Avbryt operasjon
						utFil.close();
						skrivStartListeMeny();	//Skriver forrige meny
						break;	//Case over
						}
					}
				system("pause");
				skrivStartListeMeny();	//Skriver forrige meny
				}
			else //Dersom ovelse ikke finnes
			{
				cout << "\nListe finnes ikke.\n";
				system("pause");	//Gir bruker tid til � lese input
				skrivStartListeMeny();
			}
			}
////////////////////////////STATISTIKK///////////////////
////////////////////////////Sender motatt forkortelse videre til poenger og medaljer klassen////////////////
		void sendForkort(char * forkort)
	{
			poenger.leggTilForkortelse(forkort);	//Legger til forkortelse i poengers forkortelsearray
			medaljer.leggTilForkortelse(forkort);	//Legger forkortelse til i medaljers forkortelsearray
	}