#pragma once
#include "Global.h"
#include "Ovelser.h"

///////////////Gren klasse/////////////////
class Gren : public TextElement	//Arver fra TextElement
{
private:							//privat n�dvendig data
	int* antOvelser;	//Lagring for antall ovelser
	int* antSifre;	//Lagring for antall sifre
	string* typeMaaling;	//Hvordan resultatet m�les (Poeng/Tid)
	Ovelse * ovelser[MAXOVELSER];	//�velse array
public:														//Public funksjoner
	Gren() { };											//Tom constructor
	Gren(ifstream & inn, const char * grenNvn);			//Leser fra fil baser p� navn
	Gren(const char* gnavn);							//Constructor med navn
	void display();										//Display funskjon
	void endreGren(Gren * tempGren);					//Endre gren funksjon
	void grenSkrivTilFil(ofstream & inn);				//Skriver gren til fil
	Ovelse * giOvelse(int i) { return ovelser[i]; }		//Returnerer �velse i arrayens plass
	int * giAntOvelser() { if (antOvelser != NULL)return antOvelser; else return 0; }			//Returnerer antall �velser
	void leggTilIArray(Ovelse * tempOvelse);			//Legger �velser i array
	void fjernFraArray(Ovelse * tempOvelse);	//Fjerner en enkelt ovelse
	char * giGrenNavn() { return text; }			//Returnerer navnet p� gren
	void reduserAntOvelser()	{ --(*antOvelser); }	//Subtraherer en �velse
	bool finnesOvelse(string * nvn);	//Sjekker om en �velse finnes
};