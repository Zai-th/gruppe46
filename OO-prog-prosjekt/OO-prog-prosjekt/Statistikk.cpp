#include "Statistikk.h"

///////////////////////Legg til forkortelse//////////////////////
void Statistikk::leggTilForkortelse(char * forkort)
{
	bool innlagt = false;
	for (int i = 1; innlagt == false; i++)		//looper til innlagt er true
	{
		if (forkortelser[i] == NULL)			//Dersom null
		{
			forkortelser[i] = new char;			//oppretter ny char i arrayets plass
			memcpy(forkortelser[i], forkort, strlen(forkort)+1);	//Kopierer innhold fra medsent...
			innlagt = true;											 //parameter til forkortelse i arrayets plass
		}
		else if (*forkortelser[i] == *forkort)	//Dersom forkortelse finnes
			innlagt = true;
	}
}

/////////////////////Gir skuffnummer///////////////////////
int * Statistikk::giSkuffNr(char * forkort)
{
	bool truffet = false;						//Initialiserer bool til false
	int * skuffNr = new int;					//Oppretter ny pointer
	*skuffNr = 0;
	for (int i = 1; truffet == false; i++)		//Kj�rer til nr er true
	{
		if (forkortelser[i] != NULL)			//Hvis plass i array finnes
		{
			if (*forkort == *forkortelser[i])	//Hvis parameter er lik forkortelse
			{
				*skuffNr = i;					//setter skuffnummer lik plass i array
				truffet = true;					//setter truffet lik true
			}
		}
	}
	return skuffNr;								//Returnerer skuffnummer
}