#include "Nasjon.h"

/////////////////////Leser fra fil//////////////////
Nasjon::Nasjon(ifstream & inn, char * tempC) :TextElement(tempC) 
{
	if (inn)	//Hvis fil kan �pnes
	{									//Leser Forkortelse, Navn p� nasjon, kontaktnavn,..
		string * temp = new string;		 //kontakttlf og antall deltagere fra fil
		forkort = new string;
		*forkort = string(tempC);
		while(*temp == "")				//Kj�rer s� lenge blank
		getline(inn, *temp);
		nasjonNavn = new string;		//Opretter ny pointer
		*nasjonNavn = *temp;			//Setter temp lik nasjonnavn
		getline(inn, *temp);			//leser fra temp
		kontaktNavn = new string;
		*kontaktNavn = *temp;
		kontaktTlf = new int;
		inn >> *kontaktTlf;
		antDeltagere = new int;
		inn >> *antDeltagere;
	}
	else
		cout << "Finner ikke fil.";		//Feilmelding at fil ikke kan �pnes
}

////////////////////Nasjon Constructor////////////////////
Nasjon::Nasjon(char* nvn) :TextElement(nvn)		//Tar imot forkortelse som parameter
{										//Leser inn forkortelse, Nasjonnavn, Kontaktnavn,..
	cin.ignore();						 //Kontakttlf, og antall deltagere fra bruker
	forkort = new string;
	*forkort = nvn;
	cout << "Navn paa nasjon: ";
	nasjonNavn = new string;
	getline(cin, *nasjonNavn);
	cout << "Kontaktnavn: ";			//skriver ut kontaktnavn
	kontaktNavn = new string;			//Opretter ny pointer
	getline(cin, *kontaktNavn);			//Leser kontaktnavn fra bruker
	kontaktTlf = new int;
	*kontaktTlf = sjekktlf();
	cout << "Antall deltagere: ";
	antDeltagere = new int;
	*antDeltagere = sjekkInt();
}

////////////////Endre nasjon/////////////////
void Nasjon::endreNasjon(Nasjon * tempNasjon)	//Tar imot forkortelse som parameter
{												//Leser inn nye verdier fra bruker
	cout << "Ny forkortelse: ";
	cin >> *forkort;
	cout << "Nytt navn: ";
	cin >> *nasjonNavn;
	cout << "Nytt kontaktnavn: ";				//Skriver ut kontaktnavn
	cin >> *kontaktNavn;						//Leser inn kontaktnavn fra bruker
	cout << "Nytt ";
	*kontaktTlf = sjekktlf();					//Kj�rer sjekktlf funksjon p� telefon
	cout << "Antall deltagere: ";
	*antDeltagere = sjekkInt();
}

///////////////////Skriver Nasjonverdier til fil//////////////////////
void Nasjon::NasjonSkrivTilFil(ofstream & inn) 
{									//Skriver nasjoners verdier til fil
	inn << *forkort << endl;
	inn << *nasjonNavn << endl;
	inn << *kontaktNavn << endl;	//Skriver kontaktnavn til fil
	inn << *kontaktTlf << endl;
	inn << *antDeltagere << endl;
}

///////////////////Nasjons display///////////////////
void Nasjon::display() 
{									//skriver nasjonsverdier p� skjerm
	cout << "\n" << *forkort << endl;
	cout << *nasjonNavn << endl;
	cout << *kontaktNavn << endl;	//Skriver kontaktNavn til skjerm
	cout << *kontaktTlf << endl;
	cout << *antDeltagere << endl;
}

//////////////////Nasjon Deconstructor//////////////////
Nasjon::~Nasjon()
{									//Fjerner nasjons verdier
	if (forkort)
		delete[] forkort;
	if (nasjonNavn)
		delete[] nasjonNavn;
	if (kontaktNavn)				//Hvis kontaktnavn finnes
		delete[] kontaktNavn;		//Fjerner kontaktnavn
	if (kontaktTlf)
		delete[] kontaktTlf;
	if (antDeltagere)
		delete[] antDeltagere;
}