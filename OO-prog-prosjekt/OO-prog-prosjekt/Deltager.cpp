#include "Deltager.h"
#include "Global.h"

using namespace std;
///////////////Leser inn en enkelt deltager fra fil./////////////
Deltager::Deltager(ifstream & inn, int * Nr) :NumElement(*Nr) 
{
		navn = new string;	//Lagring for navn
		nasjon = new char;	//Lagring for nasjon
		kjonn = new string;	//Lagring for kjonn
		inn.ignore();	//Ignorerer blank
		getline(inn, *navn);	//Henter navn
		inn >> nasjon;	//Henter nasjon
		inn.ignore();	//Ignorerer blank
		getline(inn, *kjonn);	//Henter kj�nn
}
//////////////////Oppretter en enkelt deltager////////////
Deltager::Deltager(int* n) : NumElement(*n)
{
	char valg;	//Lagring for bruker input
	bool sjekk = false;		//Om nasjon finnes alt
	navn = new string;
	nasjon = new char;
	kjonn = new string;
	cin.ignore();	//Ignorerer blank
	cout << "Navn: ";
	getline(cin, *navn);	//Henter navn fra bruker
	while (sjekk == false)	//Looper, krever nasjon som ikke alt finnes.
	{	
		cout << "Nasjonforkortelse: ";
		cin >> nasjon;	//Tar nasjon fra bruker
		sjekk = finnesNasjon(nasjon);	//Sjekker om nasjon alt finnes.
	}
	cout << "(M)ann eller (K)vinne: ";
	valg = lesKommando();	//Leser bruker input
	while (valg != 'M' && valg != 'K')
	valg = lesKommando();
		if (valg == 'M')
			*kjonn = "Mann";	//Hvis bruker skrev M, ellers K
		else if (valg == 'K')
			*kjonn = "Kvinne";
	cout << "\n\nDeltager registrert\n\n";
	skrivDeltagerMeny();	//Skriver deltager meny
}
//////////////Skriver all info om en enkelt deltager/////////
void Deltager::display()
{
	cout << "\nNummer: " << number;
	cout << "\nNavn: " << *navn;
	cout << "\nNasjon: " << nasjon;
	cout << "\nKjonn: " << *kjonn << "\n";
}
/////////////////////Endrer en enkelt deltager//////////
void Deltager::endreDeltager(Deltager * tempdeltager)
{
	char valg;	//Lagring for bruker input
	bool sjekk = false;
	cout << "\nNavn: ";
	cin >> *navn;	//Tar imot navn fra bruker
	while (sjekk == false)	//Looper til gyldig nasjon er gitt
	{
		cout << "\nNasjonforkortelse: ";
		cin >> nasjon;	//Leser nasjon fra bruker
		sjekk = finnesNasjon(nasjon);	//Returnerer om nasjonen er registrert
	}

	*kjonn = "N/A";
	while (*kjonn != "Mann" && *kjonn != "Kvinne") {
		cout << "\nKjonn: ";
		valg = lesKommando();	//Tar imot bruker input
		if (valg == 'M' || valg == 'K') {
			if (valg == 'M') { *kjonn = "Mann"; }	//Dersom M, ellers
			else if (valg == 'K') { *kjonn = "Kvinne"; }
			cout << "\n\nDeltager endret\n\n";
		}
		else 
		{
			cout << "Ugyldig kj�nn\n";
		}
	}
		system("pause");	//Gir bruker tid til � lese feilmelding
		skrivDeltagerMeny();	//Skriver forrige meny
}
/////////////Skriver ut info om en deltaker dersom tilh�rer gitt nasjon/////////////
void Deltager::omTropp(char * forkort) 
{
	if (*nasjon == *forkort)	//Dersom deltager tilh�rer gitt nasjon
		Deltager::display();	//Skriver ut all info om deltakeren
}
///////////////Skriver en enkelt deltaker til fil///////////////
void Deltager::deltagerSkrivTilFil(ofstream &inn)
{
	inn << number << endl;	//Skriver nummer til fil
	inn << *navn << endl;	//Skriver navn til fil
	inn << nasjon << endl;	//Skriver nasjonforkortelse til fil
	inn << *kjonn << endl;	//Skriver kjonn til fil
}
////////////////Skriver en enkelt deltager til deltagerlisten/////////////////
void Deltager::deltagerTilDeltagerListe(ofstream & utfil, Deltager *tempDeltager) {
	utfil << number << endl;	//Skriver nummer til fil
	utfil << *navn << endl;	//Skriver navn fil
	utfil << nasjon << endl;	//Skriver nasjonforkortelse til fil
}
///////////////////Skriver en enkelt deltager til resultatlisten//////////////////
void Deltager::deltagerTilResultatListe(ofstream & utfil, Deltager *tempDeltager, int *plass) {
	utfil << number << endl;	//Skriver nummer til fil
	utfil << *navn << endl;	//Skriver navn til fil
	utfil << nasjon << endl;	//Skriver nasjonforkortelse til fil
	utfil << *plass << endl;	//Skriver plassering til fil
}