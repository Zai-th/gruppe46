#include "Poeng.h"
#include "Global.h"

////////////////Display poeng///////////////////
void Poeng::displayPoeng(int skuffNr)
{
	if (poengContainer[skuffNr] != NULL)			//Hvis skuffplassen ikke er null
		cout << "Antall poeng: " << *poengContainer[skuffNr] << endl;	//skriver antall poeng for gitt skuff
	else
		cout << "Antall poeng: " << "0" << endl;
}

///////////////////Display forkort///////////////
void Poeng::displayForkort(char * forkortelse)
{
		if (forkortelse != NULL)					//Hvis forkortelsen eksisterer
		{
			cout << forkortelse << endl;			//Skriver ut forkortelse
		}
}

////////////////////Send poeng med nasjon////////////////
void Poeng::sendPoengerMedNasjoner(char * nasjon, int * poeng)
{
	int * skuffNr = giSkuffNr(nasjon);		//F�r nasjons skuffnummer
	poengContainer[*skuffNr] = new int;		//oppretter ny int p� skuffnummer
	*poengContainer[*skuffNr] = *poeng;		//gir skuffnummeret poeng
}

/////////////Nullstiller poeng///////////////
void Poeng::nullStillPoeng()
{
	int antNasjoner = giAntNasjoner();	//Henter ut antall ansjoner
	for (int i = 1; i <= antNasjoner; i++)	//Looper igjennom antall nasjoner
	{
		poengContainer[i] = NULL;	//Nullstiller poeng
	}
}