#pragma once
#include "Global.h"
#include <vector>
#include <iostream>
#include "Statistikk.h"

class Medaljer : public Statistikk	//Arver fra statistikk
{
private:
	int * antGull[MAXNASJONER];	//Antall gull for nasjoner
	int * antSolv[MAXNASJONER];	//Antall solv for nasjoner
	int * antBronsje[MAXNASJONER];	//Antall bronsje for nasjoner

public:
	void display(int * i);	//Skruver ut medaljer for i
	void sendMedaljerMedNasjoner(char * nasjon, int * gull, int * solv, int * bronsje);	//Sender medaljer med tilhørende nasjon og registrerer dette i klassen
	void displayForkort(char * forkortelser);	//Skriver ut forkortelsen for nasjonen
	void nullStillMedaljer();
};