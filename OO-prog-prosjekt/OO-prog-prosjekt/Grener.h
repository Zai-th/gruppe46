#pragma once
#include "Global.h"

/////////////Gren klasse///////////
class Grener 
{
private:						//Privat data
	int antGrener = 0;	//Lagring for antall grener
	List* grenListe = NULL;	//Liste over alle grener, initiliseres til NUL
public:								//Public funksjoner
	Grener();						//Constructor
	void display();					//Display funksjon
	List* giGrenListe();			//Returnerer liste
	void skrivGrenFil();			//Skriver gren til fil
	void mottaAnt(int ant) { antGrener = ant; }		//f�r antall grener
	void tellGren();				//plusser p� antall grener
	int giAnt() { return antGrener; }	//Returnerer antall grener
};