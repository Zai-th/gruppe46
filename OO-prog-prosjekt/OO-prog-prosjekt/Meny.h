#pragma once

#include "Global.h"
#include "Deltagere.h"
#include "Nasjoner.h"
#include "Hjelpefunksjoner.h"

using namespace std;

void hovedMeny();	//Tar input fra bruker for hovedmeny
void skrivHovedMeny();	//Displayer hovedmeny
void nasjonMeny();	//Tar input fra bruker for nasjonmeny
void skrivNasjonMeny();	//Displayer nasjonens meny
void deltagerMeny();	//Tar input fra bruker for deltagermeny
void skrivDeltagerMeny();	//Displayer deltager meny
void grenMeny();	//Tar input fra bruker for grenmeny
void skrivGrenMeny();	//Displayer grenmeny
void ovelseMeny();	//Tar input fra bruker for ovelsemeny
void skrivOvelseMeny();	//Displayer ovelse meny
void skrivStartListeMeny();	//Skriver ut startliste meny
void skrivResultatListeMeny();	//Skriver ut resultatliste meny
void startListeMeny(char * grenNvn);	//Tar input fra bruker for startliste meny
void resultatListeMeny(char * grenNvn);	//Tar input fra bruker for resultatliste meny