#pragma once
#include <iostream>

using namespace std;

int sjekkInt();	//Tar imot en int og kun en int av bruker og sender videre
char lesKommando();	//Tar imot ett gyldig tegn og kun ett og upercaser dette
int datofunk();	//Leser inn en gyldig dato fra bruker og sender dette videre
int klokkefunk();	//Leser inn klokkeslett fra bruker og sender dette videre
int sjekktlf();	//Leser inn et telefonNR i riktig intervall
int sjekksifre(string t);	//Leser inn siffere til måleresultat i gyldig intervall og sender videre