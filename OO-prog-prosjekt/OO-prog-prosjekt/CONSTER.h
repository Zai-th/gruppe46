#pragma once
const int MAXOVELSER = 10;	//Max ovelser som er tillat innenfor en gren
const int MAXNASJONER = 200;	//Max nasjoner som er tillatt � opprette
const int MAXTXT = 20;	//Max tegn tillatt ved input av int
const int MINDELTAGER = 1;	//Minimum deltagere en ovelse kan ha
const int MAXDELTAGER = 1000;	//Max deltagere en ovelse kan ha
const char STRLEN = 30;	//MAX tegn tillat ved input av char