#include "Hjelpefunksjoner.h"
#include <sstream>
#include <stdio.h>
#include <ctype.h>
#include "CONSTER.h"

//////////////Godtar int og kun int ifra bruker og sender videre//////////
int sjekkInt() {
	int i = 0;	//Lagring for input fra bruker
	bool erInt = false;	//Sjekk om input er int
	while (erInt == false)	//S� lenge ugyldig tegn er innskrevet
	{
		cin >> i;	//Leser inn input fra bruker
		if (cin.fail()) {	//Dersom input ikke er int
				std::cout << "Vennligst skriv inn en gyldig integer.." << std::endl;
			cin.clear();	//Fjerner input fra bruker
			cin.ignore();	//Ignorerer linjeskift
		}
		else //Dersom int
			erInt = true;	//Input er int
	}
	return i;	//Returnerer int
}

////////////Leser en enkelt char. Benyttes til menyvalg./////////////
char lesKommando() {              
	char ch;	//Lagring for input fra bruker
	cout << "\n\nKommando:  ";
	cin >> ch;	//Tar imot input fra bruker
	cin.ignore();	//Ignorerer linjeskift
	return (toupper(ch));	//Uppercaser input og returnerer
}

///////////Tar imot dato fra bruker og sender videre///////////
int datofunk()
{
	ostringstream oss;	//Lagring for datostreng
	int dag;	//Lagring for dag
	int maaned;	//Lagring for m�ned
	int aar;	//Lagring for �r
	int hele;	//Lagring for dag+m�ned+�r
	do
	{
		cout << "\nSkriv Dato!\n\n";
		cout << "\tDag: ";
		dag = sjekkInt();	//Tar imot input fra bruker
		if (dag < 0 || dag > 31)	//Dersom dag er mindre enn 0 eller st�rre enn 31
			cout << "Uglydig!\n";
	} while (dag < 0 || dag > 31);	//S� lenge ugyldig dag
	do
	{
		cout << "\tM�ned: ";
		maaned = sjekkInt();	//Tar imot input fra bruker
		if (maaned < 0 || maaned > 12)	//Dersom m�ned mindre enn 0 eller st�rre enn 12
			cout << "Ugyldig\n";
		while (maaned == 2 && dag > 28)	//S� lenge ugyldig m�ned
		{
			cout << "Ugyldig\n";
			cout << "M�ned: ";
			maaned = sjekkInt();	//Tar imot input fra bruker
		}
	} while (maaned < 0 || maaned > 12);	//S� lenge m�ned er ugyldig
	do
	{
		cout << "\t�r: ";
		aar = sjekkInt();	//Tar imot input fra bruker
		if (aar < 1900 || aar > 3000)	//Dersom �r er mindre enn 1900 eller st�rre enn 3000
			cout << "Ugyldig\n";
	} while (aar < 1900 || aar > 3000);	//S� lenge �r er ugyldig

	oss << dag << maaned << aar;	//Skriver dato til oss
	istringstream iss(oss.str());	//Gj�r om til string og legger i iss
	iss >> hele;	//Sender stringen til hele

	return hele;	//Returnerer hele datoen som int
}

////////////////////Tar imot gyldig klokkeslett fra bruker///////////////
int klokkefunk()
{
	ostringstream oss;	//Streamstreng lagring for klokkeslett
	int minutt;	//Lagring for minutt
	int time;	//Lagring for time
	int hele;	//Lagring for hele klokkeslett
	do
	{
		cout << "\nSkriv tid!\n\n";
		cout << "\tTime: ";
		time = sjekkInt();	//Tar input ifra bruker
	} while (time < 0 || time > 25);	//S� lenge time ugyldig
	do
	{
		cout << "\tMinutt: ";
		minutt = sjekkInt();	//Tar input ifra bruker
	} while (minutt < 0 || minutt > 60);	//S� lenge minutt er ugyldig
	oss << time << minutt;	//Sender time og minutt til oss
	istringstream iss(oss.str());	//Konverterer til stringobjekt
	iss >> hele;	//Sender innholdet til hele

	return hele;	//Returnerer hele
}

/////////////////////Tar imot telefonnummer fra bruker///////////
int sjekktlf()
{
	int nummer;	//Lagring for input fra bruker
	do
	{
		cout << "Tlfnummer: ";
		nummer = sjekkInt();	//Tar imot input fra bruker
	} while (nummer <= 10000000 || nummer >= 999999999999);	//S� lenge nummer er ugyldig
		return nummer;	//Returnerer nummer
}

//////////////////S�rger for at antall sifre tilsvarer type m�ling////////
int sjekksifre(string t)
{
	int antsifre;	//Lagring for antall sifre
	if (t == "Tid")	//Dersom medsendt parameter er Tid
	{
		do
		{
			cout << "Velg antall sifre 3(Tidel), 4(Hundredel) eller 5(Tusendel): ";
			antsifre = sjekkInt();
			if (antsifre >= 3 && antsifre <= 5)
				return antsifre;
		} while (antsifre > 5 || antsifre < 3);
		do
		{
			antsifre = sjekkInt();	//Tar imot input fra bruker
			if (antsifre >= 3 && antsifre <= 5)	//Sjekker at input er gyldig
				return antsifre;	//Returnerer antall siffer
		} while (antsifre > 5 || antsifre < 3);	//S� lenge ugyldig input
	}
	else if (t == "Poeng")	//Dersom medsendt parameter er Poeng
	{
		do
		{
			cout << "Velg antall sifre (1 eller 2): ";
			antsifre = sjekkInt();
			if (antsifre <= 2 && antsifre >= 1)
				return antsifre;
		} while (antsifre < 1 || antsifre > 2);
		do
		{
			antsifre = sjekkInt();	//Tar imot input fra bruker
			if (antsifre <= 2 && antsifre >= 1)	//Dersom gyldig input
				return antsifre;	//Returnerer antsifre
		} while (antsifre < 1 || antsifre > 2);	//S� lenge ugyldig input
	}
}