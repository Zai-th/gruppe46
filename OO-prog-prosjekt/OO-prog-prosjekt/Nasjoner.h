#pragma once
#include "ListTool2B.h"
#include <string>

using namespace std;

/////////////////Nasjoner klasse///////////////////
class Nasjoner
{
private:
	List * nasjonListe = NULL;			//Nasjoner liste, inisialisert til null
	int antNasjoner = 0;	//Antall nasjoner i listen
public:
	Nasjoner();	//Constructor for nasjoner, allokerer minne for nasjonsListe
	List * giNasjonListe() { return nasjonListe; }	//Returnerer listen over nasjoner
	void skrivNasjonFil();	//Skriver alle nasjoner til fil
	void display();								//Displayer alle nasjoner
	void tellNasjon() { antNasjoner++; }	//Adderer en nasjon til antNasjoner
	int giAnt() { return antNasjoner; }	//Returnerer antall nasjoner
	void mottaAnt(int tempAnt) { antNasjoner = tempAnt; }	//Mottar antall nasjoner og lagrer dette
};