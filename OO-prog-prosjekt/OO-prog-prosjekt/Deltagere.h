#pragma once

class Deltagere
{
private:
	List* deltagerListe;	//Liste som skal inneholde alle deltagerne
	int antDeltagere = 0;	//Holder telling p� hvor mange deltagere som finnes
public:
	Deltagere();	//Default constructor for deltagere
	List* gideltagerListe();	//Returnerer listen sin over deltakere
	void display();	//Skriver ut alle deltakere i listen
	void skrivDeltagerTilFil();	//Skriver alle deltagere til fil
	void sendAntDeltager(int tempd); //Lagrer antall deltagere til egen antDeltagere
	void tellDeltager();	//Adderer 1 til antDeltagere
	int giAntDeltagere() { return antDeltagere; }	//Returnerer antall deltagere i listen
};