#pragma once
#include "CONSTER.h"
#include <string>

class Statistikk
{
private:
	char * forkortelser[MAXNASJONER];	//Inneholder forkortelser for nasjoner
public:
	Statistikk() { };	//Default constructor
	void leggTilForkortelse(char * forkort);	//Legger en forkortelse til i sin liste
	char * giTilgang(int i) { return forkortelser[i]; }	//Returnerer forkortelsen i skuff nr i
	int * giSkuffNr(char * forkort);	//Gir skuff nummer til forkortelsen gitt
};