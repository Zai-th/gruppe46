#pragma once

#include <iostream>
#include "ListTool2B.h"
#include "Deltagere.h"
#include "Nasjoner.h"
#include "Ovelser.h"
#include "Hjelpefunksjoner.h"
#include "Meny.h"
#include "CONSTER.h"
#include <fstream>

using namespace std;

void lesFraFil();
////////////////NASJONER//////////////
int giAntNasjoner();
void nyNasjon();	//Oppretter en ny nasjon
void hentNasjonListe(List temp);	//Henter ut listen med nasjoner
void skrivNasjonListe();	//Skriver ut absolutt alle nasjoner
bool finnesNasjon(char * nasj);	//Sjekker om en nasjon eksisterer i systemet
void skrivEnNasjon();	//Skriver ut all info om en nasjon
void endreNasjon();	//Endrer en nasjons informasjon
void skrivTropp(char * forkortelse);	//Skriver ut all info om en nasjons tropp
/////////////////FILER////////////////
void skrivTilFil();	//Skriver alt til fil
/////////////////DELTAGERE////////////
void nyDeltager();	//Leser inn info om en deltager
void hentDeltagerListe(List temp);	//Henter ut deltagerlisten
void skrivDeltagerListe();	//Skriver all info p� deltagerlisten
void endreDeltager();	//Endrer info om en deltager
void skrivEnDeltager();	//Skriver all info om en enkelt deltager
void skrivDeltagere();	//Skriver ut info om deltagere dersom de tilh�rer gitt nasjon
//////////////////////GRENER//////////
void nyGren();	//Oppretter en ny gren
void hentGrenListe(List temp);	//Henter ut listen med grener
void skrivGrenListe();	//Skriver listen over alle grener
void endreGren();	//Endrer info om en gren
void skrivEnGren();	//Skriver all info om en enkelt gren
bool finnesGren(char * gren);	//Sjekker om gren med gitt navn finnes
void skrivGrenTilFil();	//Skriver alt av grener til fil
int antGren();	//Gir antall grener
/////////////MEDALJER OG POENG////////////////
void skrivPoeng();	//Skriver ut poeng for hver nasjon
void skrivMedaljer();	//Skriver ut medaljer for hver nasjon
void hentMedaljerOgPoeng();	//Henter ut medaljer og poeng ifra .RES filene
void kalkulerPoengOgMedaljer(ifstream & poengFil, int * antIFil);	//Regner ut og sorterer medaljer og poeng for hver nasjon
void nullStill();
////////////�VELSER/////////////////
void nyOvelse(char * grenNvn);	//Registrerer en ny �velse
void skrivAlleOvelser(char * nvn);	//Skriver ut alle �velser
void endreOvelse(char * grenNvn);	//Endrer en �velse
void fjernOvelse(char * grenNvn, int * ovelseNr);	//Fjerner en �velse
int giAntOvelser(char * grenNvn);	//Returnerer antall ovelser
bool finnesOvelse(int * ovelseNr, char * grenNvn);	//Sjekker om en �velse faktisk finnes
////////////LISTER///////////////////
void skrivDeltagerStartListe(char * grenNvn, int * ovelseNr);	//Skriver ut startlisten for ovelsen
void nyDeltagerListe(char * grenNvn, int * ovelseNr);	//Oppretter startliste
void endreDeltagerListe(char * grnvn, int * ovnr);	//Endrer startliste
void nyResultatListe(char * grenNvn, int * ovelseNr);	//Oppretter resultatliste
void fjernDeltagerListe(char * grenNvn, int * ovelseNr);	//Sletter startliste
void endreDeltagerListe(char * grenNvn, int * ovelseNr);	//Endrer startliste
void skrivResultatListe(char * grenNvn, int * ovelseNr);	//Skriver ut resultatlisten
void fjernResultatListe(char * grenNvn, int * ovelseNr);	//Fjerner resultatlisten
//////////////////STATISTIKK//////////////////////
void sendForkort(char * forkort);	//Sender forkortelse til poeng og medaljer