#pragma once
#include "Statistikk.h"
using namespace std;

class Poeng : public Statistikk	//Arver fra statistikk
{
private:
	int * poengContainer[MAXNASJONER];	//Antall poeng for hver nasjon
public:
	void displayPoeng(int skuffNr);	//Viser poengene til nasjon med skuffNr
	void displayForkort(char * forkortelser);	//Displayer forkortelsen for en nasjon
	void sendPoengerMedNasjoner(char * nasjon, int * poeng);	//Mottar poenger med nasjonsforkortelse og lagrer dette hos seg
	void nullStillPoeng();
};

