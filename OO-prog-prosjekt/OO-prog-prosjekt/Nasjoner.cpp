#include "Nasjoner.h"
#include "Nasjon.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

///////////////Nasjoner constructor/////////////////
Nasjoner::Nasjoner() {
	nasjonListe = new List(Sorted);		//Opretter sortert liste
	int antNasjoner = 0;				//initialiserer antnasjoner til 0
}

/////////////Nasjoner display///////////////
void Nasjoner::display() {
	nasjonListe->displayList();		//Kj�rer Listtools displaylist p� nasjonListe
	cout << "\n";
	system("pause");
}

///////////////Skriver Nasjon til fil//////////////////
void Nasjoner::skrivNasjonFil() {
	ofstream nasjonFil("NASJONER.DTA");		//Oppretter/kj�rer p� fil
	if (nasjonFil)							//Hvis fil kan �pnes
	{
		Nasjon * tempNasjon;				//Oppretter temp-objekt
		int noOfElements = nasjonListe->noOfElements();	//Henter antall elementer til en int
		nasjonFil << antNasjoner << endl;				//Skriver antall nasjoner
		for (int i = 0; i < noOfElements; i++)			//L�kke som g�r til ikke fler elementer finnes
		{
			tempNasjon = (Nasjon*)nasjonListe->removeNo(0);		//Fjerner alltid skuffe nr 0
			if (tempNasjon != NULL)								//Hvis tempnasjon ikke er null
				tempNasjon->NasjonSkrivTilFil(nasjonFil);		//kj�rer nasjonskrivtilfil
		}
		nasjonFil.close();								//Lukker fil
	}
	else cout << "\n�pning av NASJONER.DTA feilet...";			//Feilmelding hvis fil ikke kan �pnes
}