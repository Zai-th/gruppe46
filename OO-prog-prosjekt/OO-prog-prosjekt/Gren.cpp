#include "Gren.h"
#include "Ovelser.h"

//////////////Gren constructor basert p� navn///////////////////
Gren::Gren(const char * gnavn) : TextElement (gnavn)
{
	char valg;
	cout << "Maalings type (T)id eller (P)oeng: ";
	typeMaaling = new string;
	valg = lesKommando();
	while (valg != 'T' && valg != 'P')	//Whiler s� lenge rett kommando er tastet inn
		valg = lesKommando();
	if (valg == 'T')
		*typeMaaling = "Tid";
	else if (valg == 'P')
		*typeMaaling = "Poeng";
	antSifre = new int;					//Oppretter ny pointer
	*antSifre = sjekksifre(*typeMaaling);
	ovelser[MAXOVELSER] = new Ovelse;	//Opretter ovelser-array
	antOvelser = new int;
	*antOvelser = 0;
}

/////////////////Leser gren fra fil////////////////////
Gren::Gren(ifstream & inn, const char * grenNvn) :TextElement(grenNvn)
{
	antOvelser = new int;
	inn >> *antOvelser;				//Leser antall �velser
	antSifre = new int;
	inn >> *antSifre;				//Leser antall sifre
	typeMaaling = new string;
	inn >> *typeMaaling;
	Ovelse * tempOvelse = new Ovelse;
	for (int i = 1; i <= *antOvelser; i++)	//Kj�rer til antall �velser
	{
		tempOvelse->lesOvelseFil(inn);		//leser �velser
		ovelser[i] = new Ovelse;
		*ovelser[i] = *tempOvelse;	//Sender innholdet fra temp til ovelse array
	}
}

////////////////Endre gren//////////////////
void Gren::endreGren(Gren * tempGren) 
{
	cout << "\nAngi nytt grennavn: ";
	cin >> text;	//Tar navnet p� ny gren fra bruker
}

////////////////Display funksjon////////////////
void Gren::display()
{
	cout << "\nGren-Navn: " << text;
	cout << "\nAntall sifre: " << *antSifre;			//Skriver ut data
	cout << "\nType Maaling: " << *typeMaaling;
	if (antOvelser != NULL) 
	{
		cout << "\nAntall Ovelser: " << *antOvelser << endl;
		for (int i = 1; i <= *antOvelser; i++)			//Looper til antall �velser
		{
			ovelser[i]->display();						//Kj�rer �velserdisplay p� gitt �velsenr
		}
	}
}

////////////////Skriver gren til fil//////////////
void Gren::grenSkrivTilFil(ofstream & utFil)
{
	utFil << text << endl;					//Skriver gren-navn
	if (antOvelser != NULL)						//sjekker antall �velser
	utFil << *antOvelser << endl;
	else utFil << 0 << endl;
	utFil << *antSifre << endl;
	utFil << *typeMaaling << endl;
	if (antOvelser != 0)
	{
		for (int i = 1; i <= *antOvelser; i++)	//Looper til antall �velser
			ovelser[i]->skrivOvelseFil(utFil);	//Kj�rer skriv�velse p� gitt �velse
	}
}

///////////////Legger til �velser////////////////
void Gren::leggTilIArray(Ovelse * tempOvelse)
{
	if (antOvelser == NULL)					//Hvis �velse er null
	{ 
		antOvelser = new int;				//Oppretter �velse
		*antOvelser = 0;
	}
	ovelser[++*antOvelser] = tempOvelse;	//setter temp�velse til neste �velse
}

void Gren::fjernFraArray(Ovelse * tempOvelse)
{
	if (antOvelser != NULL)					//Hvis �velse er null
	{
		antOvelser = new int;				//Oppretter �velse
		*antOvelser = 0;
	}else {
		cout << "Ingen ovelser\n";
	}
	ovelser[--*antOvelser] = tempOvelse;	//setter temp�velse til neste �velse
}

bool Gren::finnesOvelse(string * nvn)
{
	bool eksistens = false;
	for (int i = 1; i <= *antOvelser; i++)
	{
		string ovelseNavn = *ovelser[i]->giOvelseNavn();
		if (ovelseNavn == *nvn)
			eksistens = true;
	}
	return eksistens;
}