#include "Global.h"
#include "Medaljer.h"

//////////////Sender medaljer med nasjonsforkortelsen og sorterer p� dette///////
void Medaljer::sendMedaljerMedNasjoner(char * nasjon, int * gull, int * solv, int * bronsje)
{
	int * skuffNr = giSkuffNr(nasjon);	//Henter skuffNr for medsendt parameter ifra statistikk klassen
	
	if (antGull[*skuffNr] != NULL && *gull != 0)	//Dersom nasjonens skuff ikke er tom
		*antGull[*skuffNr] += *gull;	//Legger til medaljer i nasjonens skuff
	else if(antGull[*skuffNr] == NULL && *gull != 0)	//Dersom skuffen er tom
	{
		antGull[*skuffNr] = new int;
		*antGull[*skuffNr] = *gull;	//Legger innhold i skuff
	}
	if (antSolv[*skuffNr] != NULL  && *solv != 0)	//Dersom nasjonens skuff ikke er tom
		*antSolv[*skuffNr] += *solv;	//Addeded medaljer til nasjonens skuff
	else if (antSolv[*skuffNr] == NULL && *solv != 0)	//Dersom skuffen er tom
	{
		antSolv[*skuffNr] = new int;
		*antSolv[*skuffNr] = *solv;	//Legger medaljer i skuff
	}
	if (antBronsje[*skuffNr] != NULL  && *bronsje != 0)	//Dersom skuffen ikke er tom
		*antBronsje[*skuffNr] += *bronsje;	//Legger til medaljer i nasjonens skuff
	else if(antBronsje[*skuffNr] == NULL && *bronsje != 0)	//Dersom nasjonens skuff er tom
	{
		antBronsje[*skuffNr] = new int;
		*antBronsje[*skuffNr] = *bronsje;	//Legger medaljer i nasjonens skuff
	}
}

/////////Displayer forkortelsen for nasjonen////////////////
void Medaljer::displayForkort(char * forkortelse)
{
	if (forkortelse != NULL)	//Dersom parameter ikke er nullptr
	{
		cout << forkortelse << endl;
	}
}

//////////Skriver ut antall medaljer for en nasjon/////////
void Medaljer::display(int * i)
{
		cout << "Antall gull: ";
		if (antGull[*i] != NULL)	//Dersom skuffen ikke er nullptr
			cout << *antGull[*i] << endl;
		else //Dersom skuffen er ubrukt
			cout << "0" << endl;
		cout << "Antall solv: ";
		if (antSolv[*i] != NULL)	//Dersom skuffen ikke er nullptr
			cout << *antSolv[*i] << endl;
		else //Dersom skuffen er ubrukt
			cout << "0" << endl;
		cout << "Antall bronsje: ";
			if (antBronsje[*i] != NULL)	//Dersom skuffen ikke er nullptr
		cout << *antBronsje[*i] << endl;
		else//Dersom skuffen er ubrukt
			cout << "0" << endl;
}

/////////////Nullstiller medaljer//////////
void Medaljer::nullStillMedaljer() {
	int antNasjoner = giAntNasjoner();	//Henter ut antall nasjoner
	for (int i = 1; i <= antNasjoner; i++) {	//Looper igjennom antall nasjoner
		antGull[i] = NULL;	//Nullstiller gull
		antSolv[i] = NULL;	//Nullstiller solv
		antBronsje[i] = NULL;	//Nullstiller bronsje
	}
}