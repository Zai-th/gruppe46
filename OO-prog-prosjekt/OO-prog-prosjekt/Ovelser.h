#pragma once
#include <string>
#include "Global.h"

///////////////klasse �velse////////////////
class Ovelse
{
private:
	int * nr;	//Lagring for nummer
	string * ovelseNavn;	//Lagring for ovelse
	int* antDeltagere;			//Lagring for antall deltagere
	int* dato;	//Lagring for dato
	int* klokkeslett;	//Lagring for klokkeslett
public:
	Ovelse() { };									//Tom constructor
	Ovelse(string * onavn, int * tempNr);				//constructor med navn og nr
	void display();									//display funksjon
	string * giOvelseNavn() { return ovelseNavn; }	//Returnerer bare �velsenavn
	int giOvelseNr() { return *nr; }				//Returnere bare �velsenr
	void endreOvelse(Ovelse * tempOvelse);			//Endre �velse funksjon
	void skrivOvelseFil(ofstream & utFil);			//Skriv �velse til fil
	void lesOvelseFil(ifstream & innFil);			//leser �velse fra fil
	bool sjekkNavn(string * tempOvelseNavn);			//sjekker navn eksistens

};